#include <OpenRO/RoApplication.h>
#include <OpenRO/Util/RoPresetColour.h>
#include <OpenRO/RoSpriteFactory.h>
#include <OpenRO/RoBackground.h>
#include <OpenRO/RoResourceLoader.h>
#include <OpenRO/RoObject3DFactory.h>
#include <OpenRO/Util/RoStringUtil.h>

namespace ro
{
#if ROCOMPILER == ROCOMPILER_MSVC_2010
	const String Application::PluginPath = "plugins/vc10/";
#elif ROCOMPILER == ROCOMPILER_MSVC_2008
	const String Application::PluginPath = "plugins/vc9/";
#elif ROCOMPILER == ROCOMPILER_MSVC_2005
	const String Application::PluginPath = "plugins/vc8/";
#elif ROCOMPILER == ROCOMPILER_MSVC_2003
	const String Application::PluginPath = "plugins/vc7/";
#else
	const String Application::PluginPath = "plugins/";
#endif // ROCOMPILER

	Application* Singleton<Application>::s_instance = 0;

	Application::Application(void)
		:mRoot(0)
		,mWindow(0)
		,mInputManager(0)
		,mKeyboard(0)
		,mMouse(0)
		,mResourceLoader(0)
		,mObject3DFactory(0)
		,mSceneManager(0)
		,mViewport(0)
		,mCamera(0)
		,mBackground(0)
		,mMiniMap(0)
		,mRenderFrames(true)
		,mCurrentTime(0.0f)
	{
	}

	Application::~Application(void)
	{
	}

	void Application::run()
	{
		if(init())
		{
			mRoot->startRendering();
		}
		mTestSprite.setNull();
		mObject3D.setNull();
		delete mBackground;
		delete mObject3DFactory;
		delete mResourceLoader;
		delete mRoot;
	}

	Ogre::RenderWindow& Application::getWindow()
	{
		assert(mWindow != 0);
		return *mWindow;
	}

	Ogre::SceneManager& Application::getSceneManager()
	{
		assert(mSceneManager != 0);
		return *mSceneManager;
	}

	Ogre::Camera& Application::getCamera()
	{
		assert(mCamera != 0);
		return *mCamera;
	}

	Ogre::Viewport& Application::getViewport()
	{
		assert(mViewport != 0);
		return *mViewport;
	}

	bool Application::init()
	{
		if (mRoot == 0)
		{
			try
			{
				// Root of Ogre
				mRoot = OGRE_NEW Ogre::Root("", "OpenRO.cfg", "OpenRO.log");
				mRoot->addFrameListener(this);
#ifdef _DEBUG
				// Load plugins
				mRoot->loadPlugin(PluginPath + "RenderSystem_GL_d");
				mRoot->loadPlugin(PluginPath + "Plugin_ParticleFX_d");
				if (!mRoot->restoreConfig() && !mRoot->showConfigDialog())
#else
				mRoot->loadPlugin(PluginPath + "RenderSystem_GL");
				mRoot->loadPlugin(PluginPath + "Plugin_ParticleFX");
				if (!mRoot->restoreConfig() && !mRoot->showConfigDialog())
#endif // _DEBUG
				{
					return false;
				}

				// Window
				mWindow = mRoot->initialise(true, "OpenRO - Ragnarok Online");
				Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);

				// Scene manager
				mSceneManager = mRoot->createSceneManager(Ogre::ST_GENERIC);
				mSceneManager->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f, 1.0f));

				// Light
				Ogre::Light* light = mSceneManager->createLight();
				light->setType(Ogre::Light::LT_DIRECTIONAL);
				light->setDirection(Ogre::Vector3(1.0f, -1.0f, 1.0f));
				light->setDiffuseColour(Ogre::ColourValue::White);
				Ogre::SceneNode* lightNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
				lightNode->attachObject(light);

				// Camera
				mCamera = mSceneManager->createCamera("MainCamera");
				mCamera->setPosition(0.0f, 0.0f, 500.0f);
				mCamera->setAutoAspectRatio(true);
				mCamera->lookAt(Ogre::Vector3::ZERO);

				// Viewport
				mViewport = mWindow->addViewport(mCamera);
				mViewport->setBackgroundColour(PresetColour::CornflowerBlue);

				// Input
				initialiseInputs();

				// RO Resource
				mResourceLoader = RO_NEW(ResourceLoader)();
				loadDataFiles();
				mObject3DFactory = RO_NEW(Object3DFactory)();

				// RO Background
				mBackground = RO_NEW(Background)();
				mBackground->init();
				mBackground->setVisible(true);

				// RO Sprite(SPR)
				StringArray spr_names = ResourceLoader::GetInstance().queryFiles(".*\\.spr");
				if (!spr_names.empty())
				{
					mTestSprite = Sprite::Load(spr_names[29]);
				}

				// RO Model(RSM)
				//std::wstring ModelName = L"도구점.rsm";
				//std::wstring ModelName = L"Data\\AlbertaShop.rsm";
				std::string ModelName = "data\\model\\ｾﾋｺ｣ｸ｣ﾅｸ\\ｵｵｱｸﾁ｡.rsm"; // EUC_KR
				ModelName = "data\\model\\ﾀﾏｺｻ\\ﾀﾏｺｻ_ｹｫｱ簔｡.rsm";
				mRsmFiles = ResourceLoader::GetInstance().queryFiles(".*\\.rsm");
				if (!mRsmFiles.empty())
				{
					if (ModelName.empty())
					{
						mRsmIndex = 1;
						mObject3D = mObject3DFactory->load(mRsmFiles[mRsmIndex]);
					}
					else
					{
						// Don't use UTF-8 encoding
						// If you need unicode filenames, you should converts to EUC_KR from UTF-16
					//	std::string utf8ModelName = StringUtil::ToUtf8(ModelName);
					//	mObject3D = mObject3DFactory->load(utf8ModelName);
						mObject3D = mObject3DFactory->load(ModelName);
					}
					if (!mObject3D.isNull())
					{
						//mObject3D->setScale(Ogre::Vector3::UNIT_SCALE * 10.0f);
					}
				}
			}
			catch(std::exception& ex)
			{
				LOG_CRITICAL("Caught Exception: "<<ex.what());
				return false;
			}
			catch(...)
			{
				return false;
			}
		}
		return true;
	}

	void Application::windowMoved( Ogre::RenderWindow* rw )
	{
	}

	void Application::windowResized( Ogre::RenderWindow* rw )
	{
		mWindowWidth = rw->getWidth();
		mWindowHeight = rw->getHeight();

		const OIS::MouseState& mouseState = mMouse->getMouseState();
		mouseState.width = mWindowWidth;
		mouseState.height = mWindowHeight;
	}

	bool Application::windowClosing( Ogre::RenderWindow* rw )
	{
		return true;
	}

	void Application::windowClosed( Ogre::RenderWindow* rw )
	{
		mRenderFrames = false;
	}

	void Application::windowFocusChange( Ogre::RenderWindow* rw )
	{
	}

	bool Application::frameStarted( const Ogre::FrameEvent& evt )
	{
		mKeyboard->capture();
		mMouse->capture();

		if (!hasKeyboardModifierPressed())
		{
			Ogre::Vector3 objectSize = mObject3D.isNull()?Ogre::Vector3::ZERO:mObject3D->getSize();
			Ogre::Vector3 scaleOffset = Ogre::Vector3::UNIT_SCALE;
			Ogre::Vector3 posOffset = Ogre::Vector3::ZERO;
			Ogre::Degree rotAngle(0.0f);
			Ogre::Vector3 rotAxis;

			// Scale
			if (mKeyboard->isKeyDown(OIS::KC_PGUP))
			{
				if (objectSize.z < 400.0f)
				{
					scaleOffset += Ogre::Vector3::UNIT_SCALE * 4.0f * evt.timeSinceLastFrame;
				}
			}
			if (mKeyboard->isKeyDown(OIS::KC_PGDOWN))
			{
				if (objectSize.z > 20.0f)
				{
					scaleOffset -= Ogre::Vector3::UNIT_SCALE * 4.0f * evt.timeSinceLastFrame;
				}
			}
			// Position
			if (mKeyboard->isKeyDown(OIS::KC_UP))
			{
				posOffset.y += 300.0f*evt.timeSinceLastFrame;
			}
			if (mKeyboard->isKeyDown(OIS::KC_DOWN))
			{
				posOffset.y -= 300.0f*evt.timeSinceLastFrame;
			}
			if (mKeyboard->isKeyDown(OIS::KC_RIGHT))
			{
				posOffset.x += 300.0f*evt.timeSinceLastFrame;
			}
			if (mKeyboard->isKeyDown(OIS::KC_LEFT))
			{
				posOffset.x -= 300.0f*evt.timeSinceLastFrame;
			}
			// Rotation
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD4))
			{
				rotAxis = Ogre::Vector3::UNIT_Y;
				rotAngle += Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD6))
			{
				rotAxis = Ogre::Vector3::UNIT_Y;
				rotAngle -= Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD2))
			{
				rotAxis = Ogre::Vector3::UNIT_X;
				rotAngle += Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD8))
			{
				rotAxis = Ogre::Vector3::UNIT_X;
				rotAngle -= Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD1))
			{
				rotAxis = Ogre::Vector3::UNIT_Z;
				rotAngle += Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD3))
			{
				rotAxis = Ogre::Vector3::UNIT_Z;
				rotAngle -= Ogre::Degree(100.0f * evt.timeSinceLastFrame);
			}

			// Reset
			if (mKeyboard->isKeyDown(OIS::KC_NUMPAD5))
			{
				mObject3D->setScale(Ogre::Vector3::UNIT_SCALE);
				mObject3D->setOrientation(Ogre::Quaternion::IDENTITY);
				mObject3D->setPosition(Ogre::Vector3::ZERO);
			}

			// Apply all operations to the model
			if (!mObject3D.isNull())
			{
				if (scaleOffset != Ogre::Vector3::UNIT_SCALE)
				{
					mObject3D->scale(scaleOffset);
				}
				if (rotAngle.valueDegrees() != 0.0f)
				{
					mObject3D->rotate(rotAngle, rotAxis);
				}
				if (posOffset != Ogre::Vector3::ZERO)
				{
					mObject3D->translate(posOffset);
				}
			}
		}
		// Update all animations
		mCurrentTime += evt.timeSinceLastFrame;
		Ogre::SceneManager::AnimationIterator animItr = mSceneManager->getAnimationIterator();
		while (animItr.hasMoreElements())
		{
			Ogre::Animation* animation = animItr.getNext();
			const Ogre::Animation::NodeTrackList& trackList = animation->_getNodeTrackList();

			Ogre::Animation::NodeTrackList::const_iterator it = trackList.begin();
			Ogre::Animation::NodeTrackList::const_iterator iend = trackList.end();

			for(; it != iend; ++it) {
				const Ogre::NodeAnimationTrack* track = it->second;
				track->getAssociatedNode()->resetToInitialState();
			}

			animation->apply(mCurrentTime);
		}
		return mRenderFrames;
	}

	bool Application::frameRenderingQueued( const Ogre::FrameEvent& evt )
	{
		return mRenderFrames;
	}

	bool Application::frameEnded( const Ogre::FrameEvent& evt )
	{
		static bool justOnce = true;
		if (justOnce)
		{
			if (!mObject3D.isNull())
			{
				LOG_CRITICAL(*mObject3D);
			}
			justOnce = false;
		}
		return mRenderFrames;
	}

	Ogre::Vector2 Application::getViewportSize() const
	{
		assert(mViewport != 0);
		Ogre::Vector2 result((Ogre::Real)mViewport->getWidth(), (Ogre::Real)mViewport->getHeight());
		return result;
	}

	Ogre::Real Application::getDisplayAspectRatio() const
	{
		assert(mViewport != 0);
		Ogre::Real aspect_ratio = (Ogre::Real)mViewport->getActualWidth()/(Ogre::Real)mViewport->getActualHeight();
		return aspect_ratio;
	}

	bool Application::keyPressed( const OIS::KeyEvent &arg )
	{
		return true;
	}

	bool Application::keyReleased( const OIS::KeyEvent &arg )
	{
		if (mKeyboard->isModifierDown(OIS::Keyboard::Ctrl))
		{
			switch (arg.key)
			{
			case OIS::KC_LEFT:
				mBackground->setPrevious();
				break;
			case OIS::KC_RIGHT:
				mBackground->setNext();
				break;
			}
		}
		else if (mKeyboard->isModifierDown(OIS::Keyboard::Alt))
		{
			switch (arg.key)
			{
			case OIS::KC_RIGHT:
				++mRsmIndex;
				if (mRsmIndex >= mRsmFiles.size())
				{
					mRsmIndex -= mRsmFiles.size();
				}
				mObject3DFactory->unload(mObject3D->getName());
				mObject3D = mObject3DFactory->load(mRsmFiles[mRsmIndex]);
				break;
			case OIS::KC_LEFT:
				if (mRsmIndex == 0)
				{
					mRsmIndex = mRsmFiles.size();
				}
				--mRsmIndex;
				mObject3DFactory->unload(mObject3D->getName());
				mObject3D = mObject3DFactory->load(mRsmFiles[mRsmIndex]);
				break;
			}
		}
		else
		{
			switch (arg.key)
			{
			case OIS::KC_ESCAPE:
				mRenderFrames = false;
				break;
			}
		}
		return true;
	}

	bool Application::mouseMoved( const OIS::MouseEvent &arg )
	{

		if (arg.state.buttonDown(OIS::MB_Left))
		{
			mObject3D->rotate(Ogre::Degree(0.50f), Ogre::Vector3(arg.state.Y.rel, arg.state.X.rel, 0));
		}
		else if (arg.state.buttonDown(OIS::MB_Right))
		{
			Ogre::Vector3 pos = mTestSprite->getPosition();
			pos.x += arg.state.X.rel / 2.f;
			pos.y -= arg.state.Y.rel / 2.f;
			mTestSprite->setPosition(pos);
		}

		return true;
//#endif
	}

	bool Application::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
	{
		return true;
	}

	bool Application::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
	{
		return true;
	}

	void Application::initialiseInputs()
	{
	//	size_t windowHandle = 0;
	//	mWindow->getCustomAttribute("WINDOW", &windowHandle);
	//	mInputManager = OIS::InputManager::createInputSystem(windowHandle);

		OIS::ParamList pl;
		size_t windowHnd = 0;
		std::ostringstream windowHndStr;
		mWindow->getCustomAttribute("WINDOW", &windowHnd);
		windowHndStr << windowHnd;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

#if defined OIS_WIN32_PLATFORM
		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_FOREGROUND" )));
		pl.insert(std::make_pair(std::string("w32_mouse"), std::string("DISCL_NONEXCLUSIVE")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_FOREGROUND")));
		pl.insert(std::make_pair(std::string("w32_keyboard"), std::string("DISCL_NONEXCLUSIVE")));
#elif defined OIS_LINUX_PLATFORM
		pl.insert(std::make_pair(std::string("x11_mouse_grab"), std::string("false")));
		pl.insert(std::make_pair(std::string("x11_mouse_hide"), std::string("false")));
		pl.insert(std::make_pair(std::string("x11_keyboard_grab"), std::string("false")));
		pl.insert(std::make_pair(std::string("XAutoRepeatOn"), std::string("true")));
#endif

		mInputManager = OIS::InputManager::createInputSystem( pl );

		mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, true));
		mKeyboard->setEventCallback(this);

		mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, true));
		mMouse->setEventCallback(this);
		unsigned int width, height, depth;
		int top, left;
		mWindow->getMetrics(width, height, depth, left, top);
		const OIS::MouseState& mouseState = mMouse->getMouseState();
		mouseState.width = width;
		mouseState.height = height;
	}

	void Application::loadDataFiles()
	{
		Ogre::ConfigFile dataFile;
		dataFile.loadDirect("Data.ini");
		Ogre::ConfigFile::SectionIterator sectionItr = dataFile.getSectionIterator();

		typedef std::map<String, String> Str2StrMap;
		Str2StrMap grfMap;
		Ogre::String sectionName, typeName, grfPath;
		while (sectionItr.hasMoreElements())
		{
			sectionName = sectionItr.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap* settings = sectionItr.getNext();
			if (sectionName == "Data")
			{
				Ogre::ConfigFile::SettingsMultiMap::iterator
					itr = settings->begin(),
					end = settings->end();
				while (itr != end)
				{
					grfMap[itr->first] = itr->second;
					++itr;
				}
			}
		}
		if (grfMap.empty())
			throw std::runtime_error("No GRF files were found under 'Data' section in Data.ini file.");
		Ogre::MapIterator<Str2StrMap> grfItr(grfMap);
		while (grfItr.hasMoreElements())
		{
			mResourceLoader->addGrf(grfItr.getNext());
		}
	}

	bool Application::hasKeyboardModifierPressed() const
	{
		return mKeyboard->isModifierDown(OIS::Keyboard::Alt)||mKeyboard->isModifierDown(OIS::Keyboard::Ctrl)||mKeyboard->isModifierDown(OIS::Keyboard::Shift);
	}
}
