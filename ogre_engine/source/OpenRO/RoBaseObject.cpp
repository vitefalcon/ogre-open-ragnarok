#define _CRT_SECURE_NO_WARNINGS
#include <OpenRO/RoBaseObject.h>
#include <NedMalloc/nedmalloc.c>
#include <exception>
#if __OPENRO_TRACK_MEMORY
#	include <fstream>
#	include <iomanip>
#endif // __OPENRO_TRACK_MEMORY

namespace ro
{
#if __OPENRO_TRACK_MEMORY
	size_t BaseObject::HeapEntry::s_heapId = 0;
	BaseObject::HeapLogger* BaseObject::sHeapLoggerInstance = 0;
#endif

	BaseObject::BaseObject(void)
	{
	}

	BaseObject::~BaseObject(void)
	{
	}

	void* BaseObject::operator new(
		unsigned int size
#if __OPENRO_TRACK_MEMORY
		,const char* file
		,const char* func
		,int line
		,const std::type_info& type
#endif // __OPENRO_TRACK_MEMORY
		)
	{
		void* obj = nedalloc::nedmalloc(size);
		if (!obj)
		{
			throw std::bad_alloc();
		}
#if __OPENRO_TRACK_MEMORY
		HeapLogger::GetInstance().logCreation(obj, HeapEntry(size, file, func, line, type, false));
#endif // __OPENRO_TRACK_MEMORY
		return obj;
	}

	void* BaseObject::operator new[](
		unsigned int size
#if __OPENRO_TRACK_MEMORY
		,const char* file
		,const char* func
		,int line
		,const std::type_info& type
#endif // __OPENRO_TRACK_MEMORY
		)
	{
		void* obj = nedalloc::nedmalloc(size);
		if (!obj)
		{
			throw std::bad_alloc();
		}
#if __OPENRO_TRACK_MEMORY
		HeapLogger::GetInstance().logCreation(obj, HeapEntry(size, file, func, line, type, true));
#endif // __OPENRO_TRACK_MEMORY
		return obj;
	}

	void BaseObject::operator delete[]( void* mem )
	{
#if __OPENRO_TRACK_MEMORY
		HeapLogger::GetInstance().logDeletion(mem);
#endif // __OPENRO_TRACK_MEMORY
		nedalloc::nedfree(mem);
	}

	void BaseObject::operator delete( void* mem )
	{
#if __OPENRO_TRACK_MEMORY
		HeapLogger::GetInstance().logDeletion(mem);
#endif // __OPENRO_TRACK_MEMORY
		nedalloc::nedfree(mem);
	}

#if __OPENRO_TRACK_MEMORY

	void BaseObject::CreateHeapLogger()
	{
		if (sHeapLoggerInstance == 0)
			sHeapLoggerInstance = new BaseObject::HeapLogger();
	}

	void BaseObject::DestroyHeapLogger()
	{
		delete sHeapLoggerInstance;
		sHeapLoggerInstance = 0;
	}

	BaseObject::HeapEntry::HeapEntry()
		:id(0)
		,heapSize(0)
		,lineNumber(0)
		,typeInfo(&typeid(void))
		,isArray(false)
	{
	}

	BaseObject::HeapEntry::HeapEntry( size_t heap_size, const std::string& file_name, const std::string& func_name, const int _line, const std::type_info& info, bool is_array )
		:id(++s_heapId)
		,heapSize(heap_size)
		,fileName(file_name)
		,functionName(func_name)
		,lineNumber(_line)
		,typeInfo(&info)
		,isArray(is_array)
	{
		std::string::size_type index_of_char = fileName.find_last_of('\\');
		if (index_of_char == std::string::npos)
		{
			index_of_char = fileName.find_last_of('/');
		}
		if (index_of_char != std::string::npos)
		{
			fileName = fileName.substr(index_of_char + 1);
		}
	}

	BaseObject::HeapEntry::HeapEntry( const HeapEntry& other )
		:id(other.id)
		,heapSize(other.heapSize)
		,fileName(other.fileName)
		,functionName(other.functionName)
		,lineNumber(other.lineNumber)
		,typeInfo(other.typeInfo)
		,isArray(other.isArray)
	{
	}

	BaseObject::HeapEntry::~HeapEntry()
	{
	}

	std::string BaseObject::HeapEntry::className()
	{
		std::string classname = typeInfo->name();
		std::string::size_type index_of_char = classname.find(' ');
		if (index_of_char != std::string::npos)
		{
			classname = classname.substr(index_of_char+1);
		}
		index_of_char = classname.find(':');
		if (index_of_char != std::string::npos)
		{
			classname = classname.substr(index_of_char+2);
		}
		if (isArray)
		{
			classname += "[]";
		}
		return classname;
	}

	const BaseObject::HeapEntry& BaseObject::HeapEntry::operator=( const HeapEntry& rhs )
	{
		id = rhs.id;
		heapSize = rhs.heapSize;
		fileName = rhs.fileName;
		functionName = rhs.functionName;
		lineNumber = rhs.lineNumber;
		typeInfo = rhs.typeInfo;
		isArray = rhs.isArray;
		return *this;
	}
	static const std::string HeapLogFile = "HeapLog.html";
#	define TABLE_BORDER 1
#	define TABLE(caption) "<br/>"<<std::endl<<"<table border=\""<<TABLE_BORDER<<"\">"<<std::endl<<"<caption><h3>"<<caption<<"</h3></caption>"<<std::endl
#	define TABLE_END "</table>"<<std::endl
#	define BR "<br/>"<<std::endl
#	define HEADER_COLOR "#FDD017"
#	define TH(val) "<th bgcolor=\""<<HEADER_COLOR<<"\">"<<val<<"</th>"<<std::endl
#	define TR "<tr>"<<std::endl
#	define TR_END "</tr>"<<std::endl
#	define TD(val) "<td>"<<val<<"</td>"<<std::endl
#	define PRINT_ADDRESS(p) "0x"<<std::setw(8)<<std::setfill('0')<<std::hex<<reinterpret_cast<size_t>(p)<<std::dec<<std::setfill(' ')<<std::setw(0)
#	define TD_COL(color,val) "<td bgcolor=\""<<color<<"\">"<<val<<"</td>"<<std::endl
#	define CREATE_COLOR "#F6358A"
#	define DELETE_COLOR "#4AA02C"
#	define CREATED(entry) TR<<TD_COL(CREATE_COLOR,"Created")<<TD(entry.className())<<TD(entry.id)<<TR_END
#	define DELETED(entry) TR<<TD_COL(DELETE_COLOR,"Deleted")<<TD(entry.className())<<TD(entry.id)<<TR_END

	BaseObject::HeapLogger* BaseObject::HeapLogger::sInstance = 0;

	BaseObject::HeapLogger::HeapLogger()
		:mTotalHeapSize(0)
	{
		time_t startTimeSecs = time(0);
		tm* startTime = localtime(&startTimeSecs);
		mStartTimeStr = asctime(startTime);
	}

	BaseObject::HeapLogger::~HeapLogger()
	{
		std::ofstream log(HeapLogFile.c_str(), std::ios::out);
		if (log.is_open())
		{
			log<<"<html>"<<std::endl;
			log<<"<head><title>OpenRO Heap Log</title></head>"<<std::endl;
			log<<"<body>"<<std::endl;
			time_t stopTime = time(0);
			tm stop_time = *localtime(&stopTime);
			if (mMap.empty())
			{
				log <<"<h1><font color=\"#254117\">NO MEMORY LEAKS DETECTED!</font></h1>"<<BR
					<<"<b>Start Time:</b> "<<mStartTimeStr<<BR
					<<"<b>Stop Time:</b> "<<asctime(&stop_time)<<BR;
			}else{
				log <<"<h1><font color=\"#800517\">FOUND LEAK OF "<<mMap.size()<<" OBJECT(S) TOTALING "<<mTotalHeapSize<<" BYTES!</font></h1>"<<BR
					<<"<b>Start Time:</b> "<<mStartTimeStr<<BR
					<<"<b>Stop Time:</b> "<<asctime(&stop_time)<<BR
					<<TABLE("List of objects leaked")
					<<TR
					<<TH("Class")
					<<TH("Instance ID")
					<<TH("Address")
					<<TH("Created In")
					<<TH("Function Name")
					<<TH("Line Number")
					<<TR_END;
				ObjectMap::iterator end = mMap.end();
				for (ObjectMap::iterator itr = mMap.begin(); itr != end; ++itr)
				{
					void* obj = itr->first;
					HeapEntry& entry = itr->second;
					log <<TR
						<<TD(entry.className())
						<<TD(entry.id)
						<<TD(PRINT_ADDRESS(obj))
						<<TD(entry.fileName)
						<<TD(entry.functionName)
						<<TD(entry.lineNumber)
						<<TR_END;
				}
				log<<TABLE_END<<"<br/>";
			}
			log <<TABLE("Activity Log")
				<<TR
				<<TH("Activity")
				<<TH("Class")
				<<TH("Instance ID")
				<<TR_END
				<<mStream.str()
				<<TABLE_END
				<<"</body>"<<std::endl<<"</html>"<<std::endl;
			log.close();
		}
	}

	BaseObject::HeapLogger& BaseObject::HeapLogger::GetInstance()
	{
		static HeapLogger instance;
		return instance;
	}

	void BaseObject::HeapLogger::logCreation( void* obj, HeapEntry entry )
	{
		mMap[obj] = entry;
		mTotalHeapSize += entry.heapSize;
		mStream<<CREATED(entry);
	}

	void BaseObject::HeapLogger::logDeletion( void* obj )
	{
		HeapEntry& entry = mMap[obj];
		mTotalHeapSize -= entry.heapSize;
		mStream<<DELETED(entry);
		mMap.erase(obj);
	}

	HeapLogCreator::HeapLogCreator()
	{
		BaseObject::CreateHeapLogger();
	}

	HeapLogCreator::~HeapLogCreator()
	{
		BaseObject::DestroyHeapLogger();
	}
#endif // __OPENRO_TRACK_MEMORY
}

//void* operator new(
//				   unsigned int size
//#ifdef _DEBUG
//				   ,const char* file
//				   ,const char* func
//				   ,int line
//				   ,const std::type_info& type
//#endif // _DEBUG )
//				   )
//{
//	void* obj = nedalloc::nedmalloc(size);
//	if (!obj)
//	{
//		throw std::bad_alloc();
//	}
//#ifdef _DEBUG
//	ro::BaseObject::HeapLogger::GetInstance().logCreation(obj, ro::BaseObject::HeapEntry(size, file, func, line, type, false));
//#endif // _DEBUG
//	return obj;
//}
//
//void* operator new[](
//					 unsigned int size
//#ifdef _DEBUG
//					 ,const char* file
//					 ,const char* func
//					 ,int line
//					 ,const std::type_info& type
//#endif // _DEBUG
//					 )
//{
//	void* obj = nedalloc::nedmalloc(size);
//	if (!obj)
//	{
//		throw std::bad_alloc();
//	}
//#ifdef _DEBUG
//	ro::BaseObject::HeapLogger::GetInstance().logCreation(obj, ro::BaseObject::HeapEntry(size, file, func, line, type, true));
//#endif // _DEBUG
//	return obj;
//}
//
//void operator delete( void* mem )
//{
//#ifdef _DEBUG
//	ro::BaseObject::HeapLogger::GetInstance().logDeletion(mem);
//#endif // _DEBUG
//	nedalloc::nedfree(mem);
//}
//
//void operator delete[]( void* mem )
//{
//#ifdef _DEBUG
//	ro::BaseObject::HeapLogger::GetInstance().logDeletion(mem);
//#endif // _DEBUG
//	nedalloc::nedfree(mem);
//}
