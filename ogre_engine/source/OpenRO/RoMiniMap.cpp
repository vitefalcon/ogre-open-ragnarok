#include <OpenRO/RoMiniMap.h>

namespace ro
{
	MiniMap::MiniMap(void)
		:mOverlay(0)
		,mImage(0)
	{
	}

	MiniMap::~MiniMap(void)
	{
	}

	void MiniMap::init()
	{
		//
	}

	void MiniMap::setVisible( bool visible )
	{
		if (mOverlay)
		{
			visible?mOverlay->show():mOverlay->hide();
		}
	}

	bool MiniMap::getVisible() const
	{
		if (mOverlay)
		{
			return mOverlay->isVisible();
		}
		return false;
	}

	void MiniMap::destroy()
	{
		//
	}
}
