#include <OpenRO/RoObject3D.h>
#include <OpenRO/RoApplication.h>
#include <OpenRO/RoResourceLoader.h>
#include <OpenRO/RoImage.h>
#include <OpenRO/RoUniqueName.h>
#include <OpenRO/Util/RoRsmMeshBuilder.h>
#include <Ogre/OgreSceneManager.h>
#include <Ogre/OgreLogManager.h>

namespace ro
{
	const Object3DPtr Object3D::Empty;

	Object3D::Object3D(const std::string& name, Ogre::SceneNode* node)
		:mName(name)
		,mNode(node)
		,mShiftHandedness(false)
	{
	}

	Object3D::~Object3D(void)
	{
		mNode->getCreator()->destroySceneNode(mNode);
	}

	void Object3D::setPosition( const Ogre::Vector3& pos )
	{
		mNode->setPosition(pos);
	}

	const Ogre::Vector3& Object3D::getPosition() const
	{
		return mNode->getPosition();
	}

	bool Object3D::isAnimated() const
	{
		return mAnimationLength > 0;
	}

	void Object3D::setScale( const Ogre::Vector3& scale )
	{
		mNode->setScale(scale);
	}

	const Ogre::Vector3& Object3D::getScale() const
	{
		return mNode->getScale();
	}

	void Object3D::setOrientation( const Ogre::Quaternion& orientation )
	{
		mNode->setOrientation(orientation);
	}

	const Ogre::Quaternion& Object3D::getOrientation() const
	{
		return mNode->getOrientation();
	}

	typedef std::vector<ImagePtr> ImageArray;

	bool Object3D::fromRsm( RSM& rsm )
	{
		int animLength = rsm.getAnimLen();
		if (animLength < 0)
			mAnimationLength = 0;
		else
			mAnimationLength = animLength;
		int shadeType = rsm.getShadeType();
		float objectAlpha = float(rsm.getAlpha())/255.0f;
		uint textureCount = rsm.getTextureCount();
		std::string texPathPrefix = "data\\texture\\";
		StringArray materialNames;
		materialNames.reserve(textureCount);
		for (uint i = 0; i < textureCount; ++i)
		{
			String textureName = texPathPrefix + rsm.getTexture(i);
			Image::Load(textureName, true, Ogre::ColourValue(1, 0, 1), 0.05f);
		//	materialNames.push_back(Util::CreateObject3DMaterial(textureName));
			materialNames.push_back(Util::CreateRsmObjectMaterial(textureName));
		}
		uint subObjectCount = rsm.getNodeCount();
		RsmNameIndexMap nameIndexMap;
		for (uint i = 0; i < subObjectCount; ++i)
			nameIndexMap[rsm.getNode(i).name] = i;

		mMainSubObject = genSubObjectName(rsm.getMainNode());
		for (uint i = 0; i < subObjectCount; ++i)
			createSubObject(rsm, i, nameIndexMap, materialNames);

		mNode->showBoundingBox(true);
		return true;
	}

	Object3D::SubObjectPtr Object3D::SubObject::CreateSubObject(const String& subObjectName, Ogre::SceneNode* parentNode, RSM& rsm, size_t rsmNodeIndex, const StringArray& materialNames)
	{
		const RSM::Node& rsmNode = rsm.getNode(rsmNodeIndex);
	//	String meshName = Util::CreateMesh(subObjectName, rsmNode, materialNames);
		String meshName = subObjectName;
		Util::CreateRsmMesh(subObjectName, rsmNode, materialNames);
		Ogre::SceneManager& sceneManager = *parentNode->getCreator();
		Ogre::Entity* entity = sceneManager.createEntity(UniqueName::Next(subObjectName), meshName);
		const RSM::Vertex& pos = rsmNode.pos;
		const RSM::Vertex& scale = rsmNode.scale;
		const RSM::Vertex& rotAxis = rsmNode.rotaxis;
		Ogre::SceneNode* node = parentNode->createChildSceneNode();
		if (rsmNode.is_main)
			node->setPosition(Ogre::Vector3::ZERO/* TODO: offset from bounding box */);
		else
			node->setPosition(Ogre::Vector3(pos.x,-pos.y,-pos.z));
		if (rsmNode.rotKeyframes.size() == 0)
			node->setOrientation(Ogre::Quaternion(Ogre::Radian(rsmNode.rotangle), Ogre::Vector3(rotAxis.x,-rotAxis.y,-rotAxis.z)));
		node->setScale(scale.x,scale.y, scale.z);
		node->setInitialState();
		Ogre::SceneNode* localTransforms = node->createChildSceneNode();
		Ogre::Animation* posAnim = 0;
		Ogre::Animation* rotAnim = 0;
		// Create animation if any
		Ogre::Real animLen = (Ogre::Real)rsm.getAnimLen();
		if (rsmNode.posKeyframes.size()) // probably position animation is not need
		{
			posAnim = sceneManager.createAnimation(subObjectName+"PosAnim", animLen / 1000.f);
			Ogre::NodeAnimationTrack* animTrack = posAnim->createNodeTrack(0, localTransforms);
			for (uint i = 0; i < rsmNode.posKeyframes.size(); ++i)
			{
				const RSM::PosKeyframe& posKeyFrame = rsmNode.posKeyframes[i];
				Ogre::TransformKeyFrame* keyFrame = animTrack->createNodeKeyFrame(posKeyFrame.frame / 1000.f);
				keyFrame->setTranslate(Ogre::Vector3(posKeyFrame.px, posKeyFrame.py, posKeyFrame.pz));
			}
		}
		if (rsmNode.rotKeyframes.size())
		{
			rotAnim = sceneManager.createAnimation(subObjectName+"RotAnim", animLen / 1000.f); // 1000 msec = 1 sec
			LOG_CRITICAL("Animation Length: "<<rsm.getAnimLen()*0.001f);
			Ogre::NodeAnimationTrack* animTrack = rotAnim->createNodeTrack(0, localTransforms);
			for (uint i = 0; i < rsmNode.rotKeyframes.size(); ++i)
			{
				const RSM::RotKeyframe& rotKeyFrame = rsmNode.rotKeyframes[i];
				Ogre::TransformKeyFrame* keyFrame = animTrack->createNodeKeyFrame(rotKeyFrame.frame / 1000.f);
				Ogre::Quaternion rotation(rotKeyFrame.qw, rotKeyFrame.qx, rotKeyFrame.qy, rotKeyFrame.qz);
				rotation = rotation.Inverse();
				keyFrame->setRotation(rotation);
			}
		}

		if (localTransforms)
			localTransforms->attachObject(entity);
		else
			node->attachObject(entity);

		return SubObjectPtr(RO_NEW(SubObject)(meshName, entity, node, localTransforms, posAnim, rotAnim));
	}

	void Object3D::createSubObject( RSM& rsm, size_t rsmNodeIndex, RsmNameIndexMap& rsmNameIndexMap, const StringArray& materialNames )
	{
		const RSM::Node& rsmNode = rsm.getNode(rsmNodeIndex);
		String subObjectName = genSubObjectName(rsmNode.name);
		if (mSubObjects.count(subObjectName) == 0)
		{
			Ogre::SceneNode* parentNode = mNode;
			String parentNodeName = rsmNode.parentname;
			SubObjectPtr subObject;
			if (parentNodeName.empty())
			{
				subObject = SubObject::CreateSubObject(subObjectName, mNode, rsm, rsmNodeIndex, materialNames);
			}
			else
			{
				createSubObject(rsm, rsmNameIndexMap[parentNodeName], rsmNameIndexMap, materialNames);
				subObject = mSubObjects[genSubObjectName(parentNodeName)]->createChild(subObjectName, rsm, rsmNodeIndex, materialNames);
			}
			mSubObjects[subObjectName] = subObject;
		}
	}

	Ogre::Vector3 Object3D::getSize() const
	{
		return mNode->_getWorldAABB().getSize();
	}

	void Object3D::translate( const Ogre::Vector3& pos )
	{
		mNode->translate(pos);
	}

	void Object3D::rotate( const Ogre::Radian& deltaAngle, const Ogre::Vector3& axis )
	{
		mNode->rotate(axis, deltaAngle);
	}

	void Object3D::scale( const Ogre::Vector3& scale )
	{
		mNode->scale(scale);
	}

	const String& Object3D::getName() const
	{
		return mName;
	}

	inline String Object3D::genSubObjectName( const String& nodeName )
	{
		StringStream ss;
		ss<<mName<<"["<<nodeName<<"]";
		return ss.str();
	}

	Object3D::SubObject::SubObject(
		const std::string& name,
		Ogre::Entity* entity,
		Ogre::SceneNode* node,
		Ogre::SceneNode* animNode,
		Ogre::Animation* posAnim,
		Ogre::Animation* rotAnim)
		:mName(name)
		,mEntity(entity)
		,mNode(node)
		,mAnimNode00(animNode)
		,mPosAnimation00(posAnim)
		,mRotAnimation00(rotAnim)
	{
	}

	Object3D::SubObject::~SubObject()
	{
		Ogre::SceneManager& sceneManager = *mNode->getCreator();
		if (mPosAnimation00)
		{
			sceneManager.destroyAnimation(mPosAnimation00->getName());
			mPosAnimation00 = 0;
		}
		if (mRotAnimation00)
		{
			sceneManager.destroyAnimation(mRotAnimation00->getName());
			mRotAnimation00 = 0;
		}
		mNode->showBoundingBox(false);
		mNode->detachAllObjects();
		mNode->removeAllChildren();
		sceneManager.destroySceneNode(mNode);
		if (mAnimNode00)
		{
			mAnimNode00->detachAllObjects();
			mAnimNode00->removeAllChildren();
			sceneManager.destroySceneNode(mAnimNode00);
		}
		mNode = 0;
		sceneManager.destroyEntity(mEntity);
		mEntity = 0;
	}

	const std::string& Object3D::SubObject::getName() const
	{
		return mName;
	}

	Ogre::SceneNode* Object3D::SubObject::getNode()
	{
		return mNode;
	}

	Ogre::Entity* Object3D::SubObject::getEntity()
	{
		return mEntity;
	}

	Ogre::SceneNode* Object3D::SubObject::getAnimationNode()
	{
		return mAnimNode00;
	}

	Object3D::SubObjectPtr Object3D::SubObject::createChild( const String& subObjectName, RSM& rsm, size_t rsmNodeIndex, const StringArray& materialNames )
	{
		SubObjectPtr childObject = CreateSubObject(subObjectName, mNode, rsm, rsmNodeIndex, materialNames);
		mChildren[subObjectName] = childObject;
		return childObject;
	}

	std::ostream& Object3D::SubObject::print( std::ostream& stream, int intendation )
	{
		std::stringstream ss;
		for (int i = 0; i < intendation; ++i)
			ss<<"\t";
		std::string intend = ss.str();
		Ogre::Degree angle, locAngle;
		Ogre::Vector3 axis, locAxis;
		mNode->getOrientation().ToAngleAxis(locAngle, locAxis);
		mNode->_getDerivedOrientation().ToAngleAxis(angle, axis);
		stream
			<<intend<<"Sub-Object"<<std::endl
			<<intend<<"Name: "<<mName<<std::endl
			<<intend<<"Size: "<<getSize()<<std::endl
			<<intend<<"Pos  (local):"<<mNode->getPosition()<<std::endl
			<<intend<<"Rot  (local):"<<locAngle<<" around "<<locAxis<<std::endl
			<<intend<<"Scale(local):"<<mNode->getScale()<<std::endl
			<<intend<<"Pos  (total):"<<mNode->_getDerivedPosition()<<std::endl
			<<intend<<"Rot  (total):"<<angle<<" around "<<axis<<std::endl
			<<intend<<"Scale(total):"<<mNode->_getDerivedScale()<<std::endl
			<<intend<<"Has Animation: "<<((mRotAnimation00 || mPosAnimation00)?"True":"False")<<std::endl
			<<intend<<"Children: "<<mChildren.size()<<std::endl;
		if (!mChildren.empty())
		{
			SubOjbectMap::iterator itr = mChildren.begin(), end = mChildren.end();
			while (itr != end)
			{
				SubObjectPtr& obj = itr->second;
				obj->print(stream, intendation + 1);
				++itr;
			}
		}
		return stream;
	}

	Ogre::Vector3 Object3D::SubObject::getSize() const
	{
		Ogre::AxisAlignedBox bounds = mEntity->getBoundingBox();
		bounds.transform(mAnimNode00->_getFullTransform());
		return bounds.getSize();
	}

	std::ostream& operator<<(std::ostream& st, const Object3D& obj)
	{
		int intendation = 0;
		st<<"Object3D"<<std::endl<<
			"Name: "<<obj.mName<<std::endl<<
			"Position: "<<obj.getPosition()<<std::endl<<
			"Rotation: "<<obj.getOrientation()<<std::endl<<
			"Scale: "<<obj.getScale()<<std::endl<<
			"Sub-Object Count: "<<obj.mSubObjects.size()<<std::endl<<
			"Main Sub-Object: "<<obj.mMainSubObject<<std::endl;

		if (obj.mSubObjects.count(obj.mMainSubObject))
		{
			obj.mSubObjects.find(obj.mMainSubObject)->second->print(st, intendation + 1);
		}
		return st;
	}
}
