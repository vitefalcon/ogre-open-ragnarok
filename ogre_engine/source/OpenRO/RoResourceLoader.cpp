#define _CRT_SECURE_NO_WARNINGS
#include <OpenRO/RoResourceLoader.h>
#include <Poco/Path.h>
#include <Ogre/OgreException.h>
#include <Ogre/OgreIteratorWrapper.h>
#include <cstdio>
#include <sys/stat.h>
#include <Poco/RegularExpression.h>
#include <Poco/DirectoryIterator.h>
#include <Ogre/OgreLogManager.h>
#include <strstream>

#include "OpenRO/Util/RoStringUtil.h"

namespace ro
{
	class OgreStreamProxy : public Ogre::DataStream
	{
	private:
		std::istream *m_stdstream;
	public:
		OgreStreamProxy(std::istream *s)
			: Ogre::DataStream(Ogre::DataStream::READ)
			, m_stdstream(s)
		{
			m_stdstream->seekg(0, std::ios::end);
			mSize = (size_t)m_stdstream->tellg();
			m_stdstream->seekg(0);
		}

		virtual size_t read(void* buf, size_t count)
		{
		//	return m_stdstream->readsome((char *)buf, count); // This method is buggy on MSVC
			std::istream::pos_type cur = m_stdstream->tellg();
			m_stdstream->read((char *)buf, count);
			return m_stdstream->tellg() - cur;
		}

		virtual void skip(long count)
		{
			m_stdstream->seekg(count, std::ios::cur);
		}

		virtual void seek(size_t pos)
		{
			m_stdstream->seekg(pos);
		}

		virtual size_t tell(void) const
		{
			return m_stdstream->tellg();
		}

		virtual bool eof(void) const
		{
			return m_stdstream->eof();
		}

		virtual void close(void)
		{
		}
	};

	ResourceLoader* Singleton<ResourceLoader>::s_instance = 0;

	ResourceLoader::ResourceLoader(void)
	{
	}

	ResourceLoader::~ResourceLoader(void)
	{
	}

	Ogre::DataStreamPtr ResourceLoader::loadDataStream( const std::string& filename )
	{
		std::istream *is = loadFile(filename);
#if 0
		is->seekg(0, std::ios::end);
		size_t size = (size_t)is->tellg();
		is->seekg(0);
		char *buf = OGRE_ALLOC_T(char, size, Ogre::MEMCATEGORY_GENERAL);
		is->read(buf, size);
 		Ogre::MemoryDataStream* memstream = new Ogre::MemoryDataStream(filename, buf, size, true, true);
		return Ogre::DataStreamPtr(memstream);
#else
		return Ogre::DataStreamPtr(new OgreStreamProxy(is));
#endif
	}

	std::istream* ResourceLoader::loadFile( const String& filename )
	{
		std::istream *result = 0;

		String _filename = StringUtil::toLower(filename);

		Poco::Path file_path(_filename);
		if (file_path.isFile())
		{
			std::fstream *fs = new std::fstream(_filename.c_str(), std::ios::in | std::ios::binary);
			if (fs->is_open())
			{
				result = fs;
			}
			else
			{
				delete fs;
			}
		}

		Ogre::VectorIterator<GrfList> grf_itr(mGrfs);
		while (grf_itr.hasMoreElements())
		{
			GrfPtr grf = grf_itr.getNext();
			std::stringbuf *buf = new std::stringbuf;
			std::iostream *data = new std::iostream(buf);
			if (grf->write(_filename, *data))
			{
				result = data;
				break;
			}
		}

		if (result == 0)
		{
			OGRE_EXCEPT(Ogre::Exception::ERR_FILE_NOT_FOUND, "File '"+filename+"' not found.", __FUNCTION__);
		}
		return result;
	}

	StringArray ResourceLoader::queryFiles( const std::string& regex_str, bool case_sensitive /*= false*/ )
	{
		StringArray result;
		int options = 0;
		if (!case_sensitive)
		{
			options = Poco::RegularExpression::RE_CASELESS;
		}
		Poco::RegularExpression regex(regex_str, options);
		Ogre::VectorIterator<GrfList> grf_itr(mGrfs);
		while (grf_itr.hasMoreElements())
		{
			GrfPtr grf = grf_itr.getNext();
			size_t file_count = grf->getCount();
			for (size_t i = 0; i < file_count; ++i)
			{
				std::string file_name = grf->getFilename(i);
				if (regex.match(file_name))
				{
					result.push_back(file_name);
				}
			}
		}
		return result;
	}

	void ResourceLoader::addGrf( const String& grf_file_path )
	{
		GRF* grf = new GRF();
		grf->open(grf_file_path);
		if (grf->isOpen())
		{
			mGrfs.push_back(GrfPtr(grf));
			Ogre::LogManager::getSingleton().logMessage(Ogre::LML_CRITICAL, "Loaded GRF '"+grf_file_path+"'.");
		}
		else
		{
			delete grf;
			Ogre::LogManager::getSingleton().logMessage(Ogre::LML_CRITICAL, "FAILED to load GRF '"+grf_file_path+"'!");
		}
	}

	InputStreamPtr ResourceLoader::loadInputStream( const String& filename )
	{
		return InputStreamPtr(loadFile(filename));
	}
}
