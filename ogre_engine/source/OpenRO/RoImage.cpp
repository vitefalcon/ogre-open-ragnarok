#include <OpenRO/RoImage.h>
#include <Ogre/OgreTextureManager.h>
#include <Ogre/OgreMaterialManager.h>
#include <Ogre/OgreHardwarePixelBuffer.h>
#include <OpenRO/RoResourceLoader.h>
#include <OpenRO/RoUniqueName.h>

namespace ro
{
	const int Image::TextureImageUsage = Ogre::TU_DEFAULT;
	const int Image::PaletteImageUsage = Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE;

	Image::Image()
		:mWidth(0)
		,mHeight(0)
		,mDepth(0)
	{
	}

	Image::~Image(void)
	{
		//if (!mTextureName.empty())
		//{
		//	Ogre::TextureManager::getSingleton().unload(mTextureName);
		//}
	}

	void Image::ColorKeyTransparency(Ogre::Image &img, const Ogre::ColourValue &keyCol, float threshold)
	{
		uint width = img.getWidth ();
		uint height = img.getHeight ();
		uchar* pixelData = OGRE_ALLOC_T (uchar, Ogre::PixelUtil::getMemorySize (width, height, 1, Ogre::PF_A8R8G8B8), Ogre::MEMCATEGORY_GENERAL);
		ulong pxDataIndex = 0, pxDataIndexStep = Ogre::PixelUtil::getNumElemBytes (Ogre::PF_A8R8G8B8); 

		for (uint y = 0; y < height; ++y) 
		{ 
			for (uint x = 0; x < width; ++x) 
			{ 
				Ogre::ColourValue pixCol = img.getColourAt (x, y, 0); 
				Ogre::ColourValue diffCol = pixCol - keyCol; 
				if ( (fabs(diffCol.r) <= threshold) && (fabs(diffCol.g) <= threshold) && (fabs(diffCol.b) <= threshold) )
				{
					pixCol = Ogre::ColourValue::ZERO;
				}
				else
				{
					pixCol.a = 1;
				}
				Ogre::PixelUtil::packColour (pixCol, Ogre::PF_A8R8G8B8, static_cast<void*> (pixelData + pxDataIndex) ); 
				pxDataIndex += pxDataIndexStep; 
			} 
		} 

		img.loadDynamicImage (pixelData, width, height, 1, Ogre::PF_A8R8G8B8, true); 
	}

	ro::ImagePtr Image::Load( const std::string& image_file_name, bool useColorKeyTransparency, const Ogre::ColourValue &keyCol, float threshold )
	{
		Ogre::TextureManager& tex_mgr = Ogre::TextureManager::getSingleton();
		Ogre::TexturePtr texture;
		if (tex_mgr.resourceExists(image_file_name)){
			texture = tex_mgr.getByName(image_file_name);
		}
		else
		{
			Ogre::DataStreamPtr image_data = ResourceLoader::GetInstance().loadDataStream(image_file_name);
			Ogre::Image img;
			img.load(image_data);

			if (useColorKeyTransparency)
				ColorKeyTransparency(img, keyCol, threshold);

			texture = Ogre::TextureManager::getSingleton().loadImage(
				image_file_name,
				Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
				img,
				Ogre::TEX_TYPE_2D,
				1);
			texture->setNumMipmaps(1);
		}
		ImagePtr image;
		if (!texture.isNull())
		{
			image = ImagePtr(RO_NEW(Image)());
			image->mWidth = texture->getWidth();
			image->mHeight = texture->getHeight();
			image->mDepth = texture->getDepth();
			image->mTextureName = texture->getName();
		}
		return image;
	}

	size_t Image::getWidth() const
	{
		return mWidth;
	}

	size_t Image::getHeight() const
	{
		return mHeight;
	}

	size_t Image::getDepth() const
	{
		return mDepth;
	}

	const std::string& Image::getName() const
	{
		return mTextureName;
	}

	void Image::setPalette( const Palette& pal, byte* palette_indices )
	{
		assert(palette_indices != 0);
		Ogre::TextureManager& tex_mgr = Ogre::TextureManager::getSingleton();
		Ogre::TexturePtr texture;
		const RGBA& transparentKey = pal[palette_indices[0]];
		if (tex_mgr.resourceExists(mTextureName))
		{
			texture = tex_mgr.getByName(mTextureName);
			Ogre::HardwarePixelBufferSharedPtr pixel_buffer = texture->getBuffer();
			pixel_buffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
			const Ogre::PixelBox& pixel_box = pixel_buffer->getCurrentLock();

			byte* pDest = static_cast<byte*>(pixel_box.data);
			size_t index = 0;
			for (size_t j = 0; j < mWidth; ++j)
			{
				for (size_t i = 0; i < mHeight; ++i)
				{
					const RGBA& color = pal[palette_indices[index++]];
					*pDest++ = color.r;
					*pDest++ = color.g;
					*pDest++ = color.b;
					if (color == transparentKey)
					{
						*pDest++ = 0;
					}
					else
					{
						*pDest++ = color.a;
					}
				}
			}

			pixel_buffer->unlock();
		}
	}

	ro::ImagePtr Image::Create( const String& name, RGBA* data, ushort width, ushort height, int texture_usage /*= TextureImageUsage*/, Ogre::PixelFormat pixel_format /*= Ogre::PF_R8G8B8A8*/ )
	{
		String image_name = name;
		if (image_name.empty())
		{
			image_name = UniqueName::Next<Image>();
		}
		Ogre::TextureManager& tex_mgr = Ogre::TextureManager::getSingleton();
		Ogre::TexturePtr texture;
		if (tex_mgr.resourceExists(image_name))
		{
			texture = tex_mgr.getByName(image_name);
		}else{
			texture = tex_mgr.createManual(image_name, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_2D,
				width, height, 1, tex_mgr.getDefaultNumMipmaps(), pixel_format, texture_usage);
			Ogre::HardwarePixelBufferSharedPtr pixel_buffer = texture->getBuffer();
			pixel_buffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
			const Ogre::PixelBox& pixel_box = pixel_buffer->getCurrentLock();

			byte* pDest = static_cast<byte*>(pixel_box.data);
			size_t index = 0;
			const RGBA& tranparentKey = data[0];
			for (size_t j = 0; j < width; ++j)
			{
				for (size_t i = 0; i < height; ++i)
				{
					const RGBA& color = data[index++];
#if 0
					*pDest++ = color.r;
					*pDest++ = color.g;
					*pDest++ = color.b;
#else
					*pDest++ = color.b;
					*pDest++ = color.g;
					*pDest++ = color.r;
#endif
					if (tranparentKey == color)
					{
						*pDest++ = 0;
					}
					else
					{
						*pDest++ = color.a;
					}
				}
			}

			pixel_buffer->unlock();
		}
		ImagePtr image;
		if (!texture.isNull())
		{
			image = ImagePtr(RO_NEW(Image)());
			image->mWidth = texture->getWidth();
			image->mHeight = texture->getHeight();
			image->mDepth = texture->getDepth();
			image->mTextureName = texture->getName();
		}
		return image;
	}

	void Image::save( const String& fileName )
	{
		Ogre::TextureManager& tex_mgr = Ogre::TextureManager::getSingleton();
		Ogre::TexturePtr texture;
		if (tex_mgr.resourceExists(mTextureName))
		{
			texture = tex_mgr.getByName(mTextureName);
			Ogre::Image image;
			texture->convertToImage(image);
			image.save(fileName);
		}
	}
}
