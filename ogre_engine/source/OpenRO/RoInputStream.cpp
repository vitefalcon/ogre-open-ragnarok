#if 0
#include <OpenRO/RoInputStream.h>
#include <NedMalloc/nedmalloc.h>

namespace ro
{
	InputStream::InputStream( std::strstreambuf* buffer, byte* bytes )
		:std::istream(buffer)
		,mBuffer(buffer)
		,mBytes(bytes)
	{
	}

	InputStream::~InputStream(void)
	{
		nedalloc::nedfree(mBytes);
		delete mBuffer;
	}

	InputStreamPtr InputStream::Create( byte* bytes, size_t size )
	{
		byte* bytesCopy = static_cast<byte*>(nedalloc::nedmalloc(size));
		memcpy(bytesCopy, bytes, size);
		std::strstreambuf* strBuffer = new std::strstreambuf(bytesCopy, size);
		InputStreamPtr inputStream(new InputStream(strBuffer, bytesCopy));
		return inputStream;
	}
}
#endif