#include <OpenRO/RoApplication.h>

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	define WIN32_LEAN_AND_MEAN
#	include "windows.h"

#ifdef _DEBUG
#	pragma comment(lib, "OgreMain_d.lib")
#	pragma comment(lib, "OIS_d.lib")
#else
#	pragma comment(lib, "OgreMain.lib")
#	pragma comment(lib, "OIS.lib")
#endif // _DEBUG

#if __OPENRO_TRACK_MEMORY
#	include <OpenRO/RoBaseObject.h>
#	include <cstdlib>

static ro::HeapLogCreator* gHeapLogCreator = 0;

void destroyHeapLogger()
{
	delete gHeapLogCreator;
	gHeapLogCreator = 0;
}
#endif // __OPENRO_TRACK_MEMORY

INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT )
#else
int main(int argc, char *argv[])
#endif
{
#if __OPENRO_TRACK_MEMORY
	gHeapLogCreator = new ro::HeapLogCreator();
	atexit(destroyHeapLogger);
#endif // __OPENRO_TRACK_MEMORY
	ro::Application* app = RO_NEW(ro::Application)();
	try
	{
		app->run();
	}
	catch(std::exception& ex)
	{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
		MessageBoxA( NULL, ex.what(), "An exception has occurred!", MB_OK | MB_ICONERROR| MB_TASKMODAL);
#else
		std::cerr << "An exception has occured: " << e.getFullDescription().c_str() << std::endl;
#endif
	}
	delete app;
#if __OPENRO_TRACK_MEMORY
	destroyHeapLogger();
#endif // __OPENRO_TRACK_MEMORY
}
