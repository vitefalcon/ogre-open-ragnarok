#include <OpenRO/RoSprite.h>
#include <OpenRO/RoResourceLoader.h>
#include <OpenRO/RoApplication.h>
#include <OpenRO/RoUniqueName.h>
#include <Ogre/OgreDataStream.h>
#include <Ogre/OgreMaterialManager.h>
#include <strstream>

namespace ro
{
	Sprite::Sprite(const String& name)
		:mName(name)
		,mSceneNode(0)
		,mBillboardSet(0)
		,mSprite(0)
		,mMaterial(0)
		,mPass(0)
		,mTexUnit(0)
	{
	}

	Sprite::~Sprite(void)
	{
	}

	ro::SpritePtr Sprite::Load( const String& file_name )
	{
		SpritePtr spr;
		InputStreamPtr input_stream = ResourceLoader::GetInstance().loadInputStream(file_name);
		SPR sprite;
		sprite.readStream(*input_stream);
		if (sprite.isValid())
		{
			String spr_name = file_name.substr(0, file_name.length() - 4);
			spr = SpritePtr(RO_NEW(Sprite)(spr_name));
			spr->init(sprite);
		}
		return spr;
	}

	void Sprite::init( SPR& spr )
	{
		if (mBillboardSet == 0)
		{
			Ogre::SceneManager& scene_manager = Application::GetInstance().getSceneManager();
			mSceneNode = scene_manager.getRootSceneNode()->createChildSceneNode();
			mBillboardSet = scene_manager.createBillboardSet(1);
			mBillboardSet->setUseAccurateFacing(true);
			mSprite = mBillboardSet->createBillboard(Ogre::Vector3::ZERO);
			uint frame_count = spr.getImageCount();
			PalettePtr palette;
			if (spr.getPal())
			{
				palette = Palette::Load(spr.getPal());
			}
			for (uint frame_index = 0; frame_index < frame_count; ++frame_index)
			{
				FramePtr frame = Frame::Create(frame_index, *this, palette, *(spr.getImage(frame_index)));
				if (!frame.isNull())
				{
					mFrames.push_back(frame);
				}
			}
			Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create(mName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
			mMaterial = material.get();
			mPass = mMaterial->getTechnique(0)->getPass(0);
			mPass->setLightingEnabled(false);
			mPass->setDepthWriteEnabled(false);
		//	mPass->setDepthCheckEnabled(false);
			mPass->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
			mTexUnit = mPass->createTextureUnitState();
			if (!mFrames.empty())
			{
				setFrame(0);
			}
		//	mMaterial->load();
		//	mMaterial->touch();
			mBillboardSet->setMaterialName(material->getName());
			mSceneNode->attachObject(mBillboardSet);
			mBillboardSet->setVisible(true);
		}
	}

	const String& Sprite::getName() const
	{
		return mName;
	}

	void Sprite::setPosition( const Ogre::Vector3& position )
	{
		mSprite->setPosition(position);
	}

	const Ogre::Vector3& Sprite::getPosition() const
	{
		return mSprite->getPosition();
	}

	void Sprite::setFrame( uint frame_index )
	{
		ImagePtr frame_image = mFrames[frame_index]->getImage();
		mTexUnit->setTextureName(frame_image->getName());
		mSprite->setDimensions((Ogre::Real)frame_image->getWidth(), (Ogre::Real)frame_image->getHeight());
	}
	Sprite::Frame::Frame( Sprite& parent, FrameType frame_type, size_t frame_index )
		:mParent(parent)
		,mType(frame_type)
		,mFrameIndex(frame_index)
	{
		StringStream ss;
		ss<<parent.getName()<<"["<<frame_index<<"]";
		const_cast<String&>(mName) = ss.str();
	}

	Sprite::Frame::~Frame()
	{
	}

	ro::ImagePtr Sprite::Frame::getImage()
	{
		return mImage;
	}

	Sprite::FrameType Sprite::Frame::getType() const
	{
		return mType;
	}

	Sprite& Sprite::Frame::getParent()
	{
		return mParent;
	}

	const Sprite& Sprite::Frame::getParent() const
	{
		return mParent;
	}

	size_t Sprite::Frame::getIndex() const
	{
		return mFrameIndex;
	}

	const String& Sprite::Frame::getName() const
	{
		return mName;
	}

	Sprite::FramePtr Sprite::Frame::Create( uint index, Sprite& parent, PalettePtr palette, const SPR::Image& img )
	{
		FramePtr frame;
		if (img.type == SPR::PalType)
		{
			PaletteFrame* pal_frame = new PaletteFrame(parent, index);
			pal_frame->setPalette(palette);
			frame = FramePtr(pal_frame);
		}else if (img.type == SPR::RgbaType){
			frame = FramePtr(new RgbaFrame(parent, index));
		}
		if (!frame.isNull())
		{
			frame->init(img);
		}
		return frame;
	}

	Sprite::PaletteFrame::PaletteFrame(Sprite& parent, size_t frame_index)
		:Frame(parent, FRAME_PALETTE, frame_index)
	{
	}

	Sprite::PaletteFrame::~PaletteFrame()
	{
	}

	void Sprite::PaletteFrame::init( const SPR::Image& img )
	{
		if (mImage.isNull())
		{
			assert(img.type == SPR::PalType);
			assert(!mPalette.isNull());
			int data_size = img.width * img.height;
			mPaletteIndex = new byte[data_size];
			memcpy(mPaletteIndex, img.data.pal, sizeof(byte)*data_size);
			RGBA* img_data = new RGBA[data_size];
			for (int i = 0; i < data_size; ++i)
			{
				img_data[i] = mPalette->at(mPaletteIndex[i]);
			}
			mImage = Image::Create(getName(), img_data, img.width, img.height, Image::PaletteImageUsage);
		}
	}

	void Sprite::PaletteFrame::setPalette( const PalettePtr& palette )
	{
		if (mPalette != palette)
		{
			if (!mImage.isNull())
			{
				mImage->setPalette(*palette, mPaletteIndex);
			}
			mPalette = palette;
		}
	}

	ro::PalettePtr Sprite::PaletteFrame::getPalette() const
	{
		return mPalette;
	}

	Sprite::RgbaFrame::RgbaFrame( Sprite& parent, size_t frame_index )
		:Frame(parent, FRAME_RGBA, frame_index)
	{
	}

	Sprite::RgbaFrame::~RgbaFrame()
	{
	}

	void Sprite::RgbaFrame::init( const SPR::Image& img )
	{
		if (mImage.isNull())
		{
			assert(img.type == SPR::RgbaType);
			int data_size = img.width * img.height;
			RGBA* img_data = new RGBA[data_size];
			for (int i = 0; i < data_size; ++i)
			{
				img_data[i] = img.data.rgba[i];
			}
			mImage = Image::Create(getName(), img_data, img.width, img.height, Image::PaletteImageUsage);
		}
	}
}
