#include <OpenRO/Util/RoRsmMeshBuilder.h>
#include <OpenRO/Util/RoCoordinateSystem.h>

#include <Ogre/OgreHardwareBufferManager.h>
#include <Ogre/OgreMaterial.h>
#include <Ogre/OgreMaterialManager.h>
#include <Ogre/OgreMesh.h>
#include <Ogre/OgreMeshManager.h>
#include <Ogre/OgrePass.h>
#include <Ogre/OgreSubMesh.h>
#include <Ogre/OgreTechnique.h>
#include <Ogre/OgreTextureUnitState.h>

using namespace Ogre;

namespace ro
{
	namespace Util
	{
		RsmMeshBuilder::RsmMeshBuilder(const RSM::Node& rsmNode)
			:mRsmNode(rsmNode)
		{
		}
		//-----------------------------------------------------------------
		RsmMeshBuilder::~RsmMeshBuilder()
		{
		}
		//-----------------------------------------------------------------
		void RsmMeshBuilder::build()
		{
			uint faceCount = mRsmNode.faces.size();
			for (uint faceIndex = 0; faceIndex < faceCount; ++faceIndex)
			{
				RsmSubMeshPtr subMesh;
				const RSM::Face& face = mRsmNode.faces[faceIndex];
				int texid = mRsmNode.textures[face.texid];
				if (mSubMeshes.count(texid))
				{
					subMesh = mSubMeshes[texid];
				}else{
					subMesh.bind(new RsmSubMesh());
					mSubMeshes[texid] = subMesh;
				}
				subMesh->addFace(face);
			}
		}
		//-----------------------------------------------------------------
		ConstSubMeshItr RsmMeshBuilder::getIterator() const
		{
			ConstSubMeshItr iterator(mSubMeshes);
			return iterator;
		}
		//-----------------------------------------------------------------
		void* RsmMeshBuilder::getVertexPtr()
		{
			return &mVertices[0];
		}
		//-----------------------------------------------------------------
		const Ogre::AxisAlignedBox& RsmMeshBuilder::getBounds() const
		{
			return mBounds;
		}
		//-----------------------------------------------------------------
		size_t RsmMeshBuilder::GetVertexSizeInBytes() // static
		{
			return sizeof(RsmVertex);
		}
		//-----------------------------------------------------------------
		RsmMeshBuilder::ConstVertexItr RsmMeshBuilder::getVertexIterator() const
		{
			return ConstVertexItr(mVertices);
		}
		//-----------------------------------------------------------------
		RsmSubMesh::RsmSubMesh()
		{
		}
		//-----------------------------------------------------------------
		void RsmSubMesh::addFace(const RSM::Face& face)
		{
			mIndexDeque.push_back(addVertexIndex(RsmVertexIndex(face.vertidx[0], face.tvertidx[0])));
			mIndexDeque.push_back(addVertexIndex(RsmVertexIndex(face.vertidx[1], face.tvertidx[1])));
			mIndexDeque.push_back(addVertexIndex(RsmVertexIndex(face.vertidx[2], face.tvertidx[2])));
		}
		//-----------------------------------------------------------------
		ushort RsmSubMesh::addVertexIndex(const RsmVertexIndex& vIdx)
		{
			ushort index = 0;
			RsmVertexIndexDeque::iterator
				vitr = mVertexIndices.begin(),
				vend = mVertexIndices.end();
			while (vitr != vend)
			{
				if (vIdx == *vitr++)
					return index;
				++index;
			}
			mVertexIndices.push_back(vIdx);
			return index;
		}
		//-----------------------------------------------------------------
		Ogre::AxisAlignedBox RsmSubMesh::buildVertexList(const RSM::Node& node, const Ogre::Matrix4& transformMatrix, const Ogre::Vector3& offset)
		{
			size_t vertexCount = mVertexIndices.size();
			mVertices.clear();
			mVertices.resize(vertexCount);
			RsmVertexIndexDeque::iterator viitr = mVertexIndices.begin(), viend = mVertexIndices.end();
			Ogre::AxisAlignedBox bounds;
			size_t index = 0;
			while (viitr != viend)
			{
				RsmVertexIndex& vindex = *viitr++;
				RsmVertex& vertex = mVertices[index++];
				const RSM::Vertex& v = node.vertices[vindex.vertexIndex];
				//vertex.pos = transformMatrix * CoordSys::ConvertPos(Ogre::Vector3(v.x, v.y, v.z), INVERT_XDIM|INVERT_ZDIM) + offset;
			//	vertex.p = transformMatrix * Ogre::Vector3(v.x, v.y, v.z) + offset;
				vertex.p = transformMatrix * Ogre::Vector3(v.x, v.y, v.z);
				if (!node.is_main || !node.is_only)
					vertex.p += offset;
				const RSM::TVertex& tvertex = node.tvertices[vindex.tvertexIndex];
				vertex.c = 0xFF000000 | tvertex.color;
				vertex.t.x = tvertex.u;
				vertex.t.y = tvertex.v;
				bounds.merge(vertex.p);
			}
			// Calculate normals
			mIndices.clear();
			mIndices.assign(mIndexDeque.begin(), mIndexDeque.end());
			size_t indexCount = mIndices.size();
			for (size_t i = 0; i < indexCount;)
			{
				RsmVertex& v1 = mVertices[mIndices[i++]];
				RsmVertex& v2 = mVertices[mIndices[i++]];
				RsmVertex& v3 = mVertices[mIndices[i++]];
				Ogre::Vector3 v1v2 = v2.p - v1.p;
				Ogre::Vector3 v1v3 = v3.p - v1.p;
				Ogre::Vector3 n = v1v2.crossProduct(v1v3);
				n.normalise();
				v1.n += n;
				v2.n += n;
				v3.n += n;
			}
			for (size_t i = 0; i < vertexCount; ++i)
			{
				mVertices[i].n.normalise();
			}
			return bounds;
		}
		//-----------------------------------------------------------------
		Ogre::AxisAlignedBox RsmSubMesh::fillData(Ogre::VertexData& vertexData, Ogre::IndexData& indexData, const RSM::Node& node)
		{
			// Create the transformation matrix
			Ogre::Matrix4 transformMatrix(
				node.offsetMT[ 0], node.offsetMT[ 1], node.offsetMT[ 2], 0,
				node.offsetMT[ 3], node.offsetMT[ 4], node.offsetMT[ 5], 0,
				node.offsetMT[ 6], node.offsetMT[ 7], node.offsetMT[ 8], 0,
				             0.0f,              0.0f,              0.0f,             1.0f);
			//CoordSys::ConvertTransformMatrix(transformMatrix, INVERT_SCALE_Y|INVERT_ZDIM);
			// Calculate normals
			Ogre::AxisAlignedBox bounds = buildVertexList(node, transformMatrix, Ogre::Vector3(node.offsetMT[9], -node.offsetMT[10], -node.offsetMT[11]));
			size_t vertexCount = mVertexIndices.size();
			vertexData.vertexCount = vertexCount;
			vertexData.vertexStart = 0;
			// Define the vertex declaration
			Ogre::VertexDeclaration& vdecl = *vertexData.vertexDeclaration;
			int offset = 0;
			int float3Size = Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT3);
			int float2Size = Ogre::VertexElement::getTypeSize(Ogre::VET_FLOAT2);
			int colourSize = Ogre::VertexElement::getTypeSize(Ogre::VET_COLOUR);
			vdecl.addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_POSITION);
			offset += float3Size;
			vdecl.addElement(0, offset, Ogre::VET_FLOAT3, Ogre::VES_NORMAL);
			offset += float3Size;
			vdecl.addElement(0, offset, Ogre::VET_COLOUR, Ogre::VES_DIFFUSE);
			offset += colourSize;
			vdecl.addElement(0, offset, Ogre::VET_FLOAT2, Ogre::VES_TEXTURE_COORDINATES);
			// Create vertex buffer
			Ogre::HardwareBufferManager& hwBufferManager = Ogre::HardwareBufferManager::getSingleton();
			Ogre::HardwareVertexBufferSharedPtr vbuf = hwBufferManager.createVertexBuffer(
				vdecl.getVertexSize(0),
				vertexCount,
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				false);
			vbuf->writeData(0, vbuf->getSizeInBytes(), &mVertices[0], true);
			// Bind the vertex buffer
			vertexData.vertexBufferBinding->setBinding(0, vbuf);
			// Fill the index buffer
			Ogre::HardwareIndexBufferSharedPtr ibuf = hwBufferManager.createIndexBuffer(
				Ogre::HardwareIndexBuffer::IT_16BIT,
				mIndices.size(),
				Ogre::HardwareBuffer::HBU_STATIC_WRITE_ONLY,
				false);
			ibuf->writeData(0, ibuf->getSizeInBytes(), &mIndices[0], true);
			indexData.indexBuffer = ibuf;
			indexData.indexStart = 0;
			indexData.indexCount = mIndices.size();
			return bounds;
		}
		//-----------------------------------------------------------------
		void CreateRsmMesh(const std::string& subMeshName, const RSM::Node& node, const StringArray& materialNames)
		{
			Ogre::MeshManager& meshManager = Ogre::MeshManager::getSingleton();
			if (!meshManager.resourceExists(subMeshName))
			{
				RsmMeshBuilder meshBuilder(node);
				meshBuilder.build();
				Ogre::MeshPtr mesh = meshManager.createManual(
					subMeshName,
					Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
				ConstSubMeshItr rsmSubMeshItr = meshBuilder.getIterator();
				Ogre::AxisAlignedBox meshBounds;
				while (rsmSubMeshItr.hasMoreElements())
				{
					ushort texId = rsmSubMeshItr.peekNextKey();
					RsmSubMesh& rsmSubMesh = *rsmSubMeshItr.getNext();
					Ogre::SubMesh* subMesh = mesh->createSubMesh();
					subMesh->useSharedVertices = false;
					if (!subMesh->vertexData)
						subMesh->vertexData = OGRE_NEW Ogre::VertexData();
					if (!subMesh->indexData)
						subMesh->indexData = OGRE_NEW Ogre::IndexData();
					meshBounds.merge(rsmSubMesh.fillData(*subMesh->vertexData, *subMesh->indexData, node));
					subMesh->setMaterialName(materialNames[texId]);
				}
				mesh->_setBounds(meshBounds);
				mesh->_setBoundingSphereRadius(Ogre::Math::boundingRadiusFromAABB(meshBounds));
				mesh->load(false);
			}
		}
		//-----------------------------------------------------------------
		const RsmVertex RsmVertex::Zero;
		//-----------------------------------------------------------------
		RsmVertex::RsmVertex()
			:p(Ogre::Vector3::ZERO)
			,n(Ogre::Vector3::ZERO)
			,c(0)
			,t(Ogre::Vector2::ZERO)
		{
		}
		//-----------------------------------------------------------------
		RsmSubMesh::Triangle::Triangle()
		{
			memset(vertexIndices, 0, sizeof(vertexIndices));
		}
		//-----------------------------------------------------------------
		RsmSubMesh::Triangle::Triangle(const Triangle& rhs)
		{
			memcpy(vertexIndices, rhs.vertexIndices, sizeof(vertexIndices));
		}
		//-----------------------------------------------------------------
		RsmSubMesh::Triangle& RsmSubMesh::Triangle::operator=(const Triangle& rhs)
		{
			memcpy(vertexIndices, rhs.vertexIndices, sizeof(vertexIndices));
			return *this;
		}
		//-----------------------------------------------------------------
		std::string CreateRsmObjectMaterial(const std::string& textureName)
		{
			Ogre::MaterialManager& materialManager = Ogre::MaterialManager::getSingleton();
			if (!materialManager.resourceExists(textureName))
			{
				Ogre::MaterialPtr material = materialManager.create(
					textureName,
					Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
				// Technique settings
				Ogre::Technique* technique_0 = material->getTechnique(0);
				technique_0->setLightingEnabled(true);
				// Pass settings
				Ogre::Pass* pass_0 = technique_0->getPass(0);
				pass_0->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
				pass_0->setAlphaRejectSettings(Ogre::CMPF_GREATER_EQUAL, 128);
				pass_0->setVertexColourTracking(Ogre::TVC_DIFFUSE);
				pass_0->setCullingMode(Ogre::CULL_NONE);
				// Texture Unit settings
				Ogre::TextureUnitState* texUnit_0 = 0;
				if (pass_0->getNumTextureUnitStates())
					texUnit_0 = pass_0->getTextureUnitState(0);
				else
					texUnit_0 = pass_0->createTextureUnitState();
				texUnit_0->setTextureName(textureName);
			}
			return textureName;
		}
		//-----------------------------------------------------------------
		//-----------------------------------------------------------------
		RsmVertexIndex::RsmVertexIndex(ushort vertexIdx, ushort tvertexIdx)
			:vertexIndex(vertexIdx)
			,tvertexIndex(tvertexIdx)
		{
		}
		//-----------------------------------------------------------------
		bool RsmVertexIndex::operator<(const RsmVertexIndex& rhs) const
		{
			return vertexIndex < rhs.vertexIndex && tvertexIndex < rhs.tvertexIndex;
		}
		//-----------------------------------------------------------------
		bool RsmVertexIndex::operator == (const RsmVertexIndex& rhs) const
		{
			return (vertexIndex == rhs.vertexIndex) && (tvertexIndex == rhs.tvertexIndex);
		}
		//-----------------------------------------------------------------
		bool RsmVertexIndex::operator != (const RsmVertexIndex& rhs) const
		{
			return (vertexIndex != rhs.vertexIndex) || (tvertexIndex != rhs.tvertexIndex);
		}
	}
}
