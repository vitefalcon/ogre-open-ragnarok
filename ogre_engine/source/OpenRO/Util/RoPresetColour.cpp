#include <OpenRO/Util/RoPresetColour.h>
#include <sstream>

using namespace Ogre;

namespace ro
{
	class CreateColourMap
	{
		PresetColour::ColourMap m_map;
	public:
		CreateColourMap(const NamedColour& named_colour)
		{
			m_map[named_colour.getName()] = &named_colour;
		}

		CreateColourMap& operator () (const NamedColour& named_colour)
		{
			m_map[named_colour.getName()] = &named_colour;
			return *this;
		}

		operator PresetColour::ColourMap()
		{
			return m_map;
		}

	};
	//---------------------------------------------------------------------
	const NamedColour PresetColour::AliceBlue("AliceBlue", ColourValue(240.0f/255.0f, 248.0f/255.0f, 1.0f));
	const NamedColour PresetColour::AntiqueWhite("AntiqueWhite", ColourValue(250.0f/255.0f, 235.0f/255.0f, 215.0f/255.0f));
	const NamedColour PresetColour::Aqua("Aqua", ColourValue(0.0f, 1.0f, 1.0f));
	const NamedColour PresetColour::Aquamarine("Aquamarine", ColourValue(127.0f/255.0f, 1.0f, 212.0f/255.0f));
	const NamedColour PresetColour::Azure("Azure", ColourValue(240.0f/255.0f, 1.0f, 1.0f));
	const NamedColour PresetColour::Beige("Beige", ColourValue(245.0f/255.0f, 245.0f/255.0f, 220.0f/255.0f));
	const NamedColour PresetColour::Bisque("Bisque", ColourValue(1.0f, 228.0f/255.0f, 196.0f/255.0f));
	const NamedColour PresetColour::Black("Black", ColourValue::Black);
	const NamedColour PresetColour::BlanchedAlmond("BlanchedAlmond", ColourValue(1.0f, 235.0f/255.0f, 205.0f/255.0f));
	const NamedColour PresetColour::Blue("Blue", ColourValue::Blue);
	const NamedColour PresetColour::BlueViolet("BlueViolet", ColourValue(138.0f/255.0f, 43.0f/255.0f, 226.0f/255.0f));
	const NamedColour PresetColour::Brown("Brown", ColourValue(165.0f/255.0f, 42.0f/255.0f, 42.0f/255.0f));
	const NamedColour PresetColour::BurlyWood("BurlyWood", ColourValue(222.0f/255.0f, 184.0f/255.0f, 135.0f/255.0f));
	const NamedColour PresetColour::CadetBlue("CadetBlue", ColourValue(95.0f/255.0f, 158.0f/255.0f, 160.0f/255.0f));
	const NamedColour PresetColour::Chartreuse("Chartreuse", ColourValue(127.0f/255.0f, 1.0f, 0.0f));
	const NamedColour PresetColour::Chocolate("Chocolate", ColourValue(210.0f/255.0f, 105.0f/255.0f, 30.0f/255.0f));
	const NamedColour PresetColour::Coral("Coral", ColourValue(1.0f, 127.0f/255.0f, 80.0f/255.0f));
	const NamedColour PresetColour::CornflowerBlue("CornflowerBlue", ColourValue(100.0f/255.0f, 149.0f/255.0f, 237.0f/255.0f));
	const NamedColour PresetColour::Cornsilk("Cornsilk", ColourValue(1.0f, 248.0f/255.0f, 220.0f/255.0f));
	const NamedColour PresetColour::Crimson("Crimson", ColourValue(220.0f/255.0f, 20.0f/255.0f, 60.0f/255.0f));
	const NamedColour PresetColour::Cyan("Cyan", ColourValue(0.0f, 1.0f, 1.0f));
	const NamedColour PresetColour::DarkBlue("DarkBlue", ColourValue(0.0f, 0.0f, 139.0f/255.0f));
	const NamedColour PresetColour::DarkCyan("DarkCyan", ColourValue(0.0f, 139.0f/255.0f, 139.0f/255.0f));
	const NamedColour PresetColour::DarkGoldenrod("DarkGoldenrod", ColourValue(184.0f/255.0f, 134.0f/255.0f, 11.0f/255.0f));
	const NamedColour PresetColour::DarkGray("DarkGray", ColourValue(169.0f/255.0f, 169.0f/255.0f, 169.0f/255.0f));
	const NamedColour PresetColour::DarkGreen("DarkGreen", ColourValue(0.0f, 100.0f/255.0f, 0.0f));
	const NamedColour PresetColour::DarkKhaki("DarkKhaki", ColourValue(189.0f/255.0f, 183.0f/255.0f, 107.0f/255.0f));
	const NamedColour PresetColour::DarkMagenta("DarkMagenta", ColourValue(139.0f/255.0f, 0.0f, 139.0f/255.0f));
	const NamedColour PresetColour::DarkOliveGreen("DarkOliveGreen", ColourValue(85.0f/255.0f, 107.0f/255.0f, 47.0f/255.0f));
	const NamedColour PresetColour::DarkOrange("DarkOrange", ColourValue(1.0f, 140.0f/255.0f, 0.0f));
	const NamedColour PresetColour::DarkOrchid("DarkOrchid", ColourValue(153.0f/255.0f, 50.0f/255.0f, 204.0f/255.0f));
	const NamedColour PresetColour::DarkRed("DarkRed", ColourValue(139.0f/255.0f, 0.0f, 0.0f));
	const NamedColour PresetColour::DarkSalmon("DarkSalmon", ColourValue(233.0f/255.0f, 150.0f/255.0f, 122.0f/255.0f));
	const NamedColour PresetColour::DarkSeaGreen("DarkSeaGreen", ColourValue(143.0f/255.0f, 188.0f/255.0f, 139.0f/255.0f));
	const NamedColour PresetColour::DarkSlateBlue("DarkSlateBlue", ColourValue(72.0f/255.0f, 61.0f/255.0f, 139.0f/255.0f));
	const NamedColour PresetColour::DarkSlateGray("DarkSlateGray", ColourValue(47.0f/255.0f, 79.0f/255.0f, 79.0f/255.0f));
	const NamedColour PresetColour::DarkTurquoise("DarkTurquoise", ColourValue(0.0f, 206.0f/255.0f, 209.0f/255.0f));
	const NamedColour PresetColour::DarkViolet("DarkViolet", ColourValue(148.0f/255.0f, 0.0f, 211.0f/255.0f));
	const NamedColour PresetColour::DeepPink("DeepPink", ColourValue(1.0f, 20.0f/255.0f, 147.0f/255.0f));
	const NamedColour PresetColour::DeepSkyBlue("DeepSkyBlue", ColourValue(0.0f, 191.0f/255.0f, 1.0f));
	const NamedColour PresetColour::DimGray("DimGray", ColourValue(105.0f/255.0f, 105.0f/255.0f, 105.0f/255.0f));
	const NamedColour PresetColour::DodgerBlue("DodgerBlue", ColourValue(30.0f/255.0f, 144.0f/255.0f, 1.0f));
	const NamedColour PresetColour::Firebrick("Firebrick", ColourValue(178.0f/255.0f, 34.0f/255.0f, 34.0f/255.0f));
	const NamedColour PresetColour::FloralWhite("FloralWhite", ColourValue(1.0f, 250.0f/255.0f, 240.0f/255.0f));
	const NamedColour PresetColour::ForestGreen("ForestGreen", ColourValue(34.0f/255.0f, 139.0f/255.0f, 34.0f/255.0f));
	const NamedColour PresetColour::Fuchsia("Fuchsia", ColourValue(1.0f, 0.0f, 1.0f));
	const NamedColour PresetColour::Gainsboro("Gainsboro", ColourValue(220.0f/255.0f, 220.0f/255.0f, 220.0f/255.0f));
	const NamedColour PresetColour::GhostWhite("GhostWhite", ColourValue(248.0f/255.0f, 248.0f/255.0f, 1.0f));
	const NamedColour PresetColour::Gold("Gold", ColourValue(1.0f, 215.0f/255.0f, 0.0f));
	const NamedColour PresetColour::Goldenrod("Goldenrod", ColourValue(218.0f/255.0f, 165.0f/255.0f, 32.0f/255.0f));
	const NamedColour PresetColour::Gray("Gray", ColourValue(128.0f/255.0f, 128.0f/255.0f, 128.0f/255.0f));
	const NamedColour PresetColour::Green("Green", ColourValue(0.0f, 128.0f/255.0f, 0.0f));
	const NamedColour PresetColour::GreenYellow("GreenYellow", ColourValue(173.0f/255.0f, 1.0f, 47.0f/255.0f));
	const NamedColour PresetColour::Honeydew("Honeydew", ColourValue(240.0f/255.0f, 1.0f, 240.0f/255.0f));
	const NamedColour PresetColour::HotPink("HotPink", ColourValue(1.0f, 105.0f/255.0f, 180.0f/255.0f));
	const NamedColour PresetColour::IndianRed("IndianRed", ColourValue(205.0f/255.0f, 92.0f/255.0f, 92.0f/255.0f));
	const NamedColour PresetColour::Indigo("Indigo", ColourValue(75.0f/255.0f, 0.0f, 130.0f/255.0f));
	const NamedColour PresetColour::Ivory("Ivory", ColourValue(1.0f, 1.0f, 240.0f/255.0f));
	const NamedColour PresetColour::Khaki("Khaki", ColourValue(240.0f/255.0f, 230.0f/255.0f, 140.0f/255.0f));
	const NamedColour PresetColour::Lavender("Lavender", ColourValue(230.0f/255.0f, 230.0f/255.0f, 250.0f/255.0f));
	const NamedColour PresetColour::LavenderBlush("LavenderBlush", ColourValue(1.0f, 240.0f/255.0f, 245.0f/255.0f));
	const NamedColour PresetColour::LawnGreen("LawnGreen", ColourValue(124.0f/255.0f, 252.0f/255.0f, 0.0f));
	const NamedColour PresetColour::LemonChiffon("LemonChiffon", ColourValue(1.0f, 250.0f/255.0f, 205.0f/255.0f));
	const NamedColour PresetColour::LightBlue("LightBlue", ColourValue(173.0f/255.0f, 216.0f/255.0f, 230.0f/255.0f));
	const NamedColour PresetColour::LightCoral("LightCoral", ColourValue(240.0f/255.0f, 128.0f/255.0f, 128.0f/255.0f));
	const NamedColour PresetColour::LightCyan("LightCyan", ColourValue(224.0f/255.0f, 1.0f, 1.0f));
	const NamedColour PresetColour::LightGoldenrodYellow("LightGoldenrodYellow", ColourValue(250.0f/255.0f, 250.0f/255.0f, 210.0f/255.0f));
	const NamedColour PresetColour::LightGray("LightGray", ColourValue(211.0f/255.0f, 211.0f/255.0f, 211.0f/255.0f));
	const NamedColour PresetColour::LightGreen("LightGreen", ColourValue(144.0f/255.0f, 238.0f/255.0f, 144.0f/255.0f));
	const NamedColour PresetColour::LightPink("LightPink", ColourValue(1.0f, 182.0f/255.0f, 193.0f/255.0f));
	const NamedColour PresetColour::LightSalmon("LightSalmon", ColourValue(1.0f, 160.0f/255.0f, 122.0f/255.0f));
	const NamedColour PresetColour::LightSeaGreen("LightSeaGreen", ColourValue(32.0f/255.0f, 178.0f/255.0f, 170.0f/255.0f));
	const NamedColour PresetColour::LightSkyBlue("LightSkyBlue", ColourValue(135.0f/255.0f, 206.0f/255.0f, 250.0f/255.0f));
	const NamedColour PresetColour::LightSlateGray("LightSlateGray", ColourValue(119.0f/255.0f, 136.0f/255.0f, 153.0f/255.0f));
	const NamedColour PresetColour::LightSteelBlue("LightSteelBlue", ColourValue(176.0f/255.0f, 196.0f/255.0f, 222.0f/255.0f));
	const NamedColour PresetColour::LightYellow("LightYellow", ColourValue(1.0f, 1.0f, 224.0f/255.0f));
	const NamedColour PresetColour::Lime("Lime", ColourValue(0.0f, 1.0f, 0.0f));
	const NamedColour PresetColour::LimeGreen("LimeGreen", ColourValue(50.0f/255.0f, 205.0f/255.0f, 50.0f/255.0f));
	const NamedColour PresetColour::Linen("Linen", ColourValue(250.0f/255.0f, 240.0f/255.0f, 230.0f/255.0f));
	const NamedColour PresetColour::Magenta("Magenta", ColourValue(1.0f, 0.0f, 1.0f));
	const NamedColour PresetColour::Maroon("Maroon", ColourValue(128.0f/255.0f, 0.0f, 0.0f));
	const NamedColour PresetColour::MediumAquamarine("MediumAquamarine", ColourValue(102.0f/255.0f, 205.0f/255.0f, 170.0f/255.0f));
	const NamedColour PresetColour::MediumBlue("MediumBlue", ColourValue(0.0f, 0.0f, 205.0f/255.0f));
	const NamedColour PresetColour::MediumOrchid("MediumOrchid", ColourValue(186.0f/255.0f, 85.0f/255.0f, 211.0f/255.0f));
	const NamedColour PresetColour::MediumPurple("MediumPurple", ColourValue(147.0f/255.0f, 112.0f/255.0f, 219.0f/255.0f));
	const NamedColour PresetColour::MediumSeaGreen("MediumSeaGreen", ColourValue(60.0f/255.0f, 179.0f/255.0f, 113.0f/255.0f));
	const NamedColour PresetColour::MediumSlateBlue("MediumSlateBlue", ColourValue(123.0f/255.0f, 104.0f/255.0f, 238.0f/255.0f));
	const NamedColour PresetColour::MediumSpringGreen("MediumSpringGreen", ColourValue(0.0f, 250.0f/255.0f, 154.0f/255.0f));
	const NamedColour PresetColour::MediumTurquoise("MediumTurquoise", ColourValue(72.0f/255.0f, 209.0f/255.0f, 204.0f/255.0f));
	const NamedColour PresetColour::MediumVioletRed("MediumVioletRed", ColourValue(199.0f/255.0f, 21.0f/255.0f, 133.0f/255.0f));
	const NamedColour PresetColour::MidnightBlue("MidnightBlue", ColourValue(25.0f/255.0f, 25.0f/255.0f, 112.0f/255.0f));
	const NamedColour PresetColour::MintCream("MintCream", ColourValue(245.0f/255.0f, 1.0f, 250.0f/255.0f));
	const NamedColour PresetColour::MistyRose("MistyRose", ColourValue(1.0f, 228.0f/255.0f, 225.0f/255.0f));
	const NamedColour PresetColour::Moccasin("Moccasin", ColourValue(1.0f, 228.0f/255.0f, 181.0f/255.0f));
	const NamedColour PresetColour::NavajoWhite("NavajoWhite", ColourValue(1.0f, 222.0f/255.0f, 173.0f/255.0f));
	const NamedColour PresetColour::Navy("Navy", ColourValue(0.0f, 0.0f, 128.0f/255.0f));
	const NamedColour PresetColour::OldLace("OldLace", ColourValue(253.0f/255.0f, 245.0f/255.0f, 230.0f/255.0f));
	const NamedColour PresetColour::Olive("Olive", ColourValue(128.0f/255.0f, 128.0f/255.0f, 0.0f));
	const NamedColour PresetColour::OliveDrab("OliveDrab", ColourValue(107.0f/255.0f, 142.0f/255.0f, 35.0f/255.0f));
	const NamedColour PresetColour::Orange("Orange", ColourValue(1.0f, 165.0f/255.0f, 0.0f));
	const NamedColour PresetColour::OrangeRed("OrangeRed", ColourValue(1.0f, 69.0f/255.0f, 0.0f));
	const NamedColour PresetColour::Orchid("Orchid", ColourValue(218.0f/255.0f, 112.0f/255.0f, 214.0f/255.0f));
	const NamedColour PresetColour::PaleGoldenrod("PaleGoldenrod", ColourValue(238.0f/255.0f, 232.0f/255.0f, 170.0f/255.0f));
	const NamedColour PresetColour::PaleGreen("PaleGreen", ColourValue(152.0f/255.0f, 251.0f/255.0f, 152.0f/255.0f));
	const NamedColour PresetColour::PaleTurquoise("PaleTurquoise", ColourValue(175.0f/255.0f, 238.0f/255.0f, 238.0f/255.0f));
	const NamedColour PresetColour::PaleVioletRed("PaleVioletRed", ColourValue(219.0f/255.0f, 112.0f/255.0f, 147.0f/255.0f));
	const NamedColour PresetColour::PapayaWhip("PapayaWhip", ColourValue(1.0f, 239.0f/255.0f, 213.0f/255.0f));
	const NamedColour PresetColour::PeachPuff("PeachPuff", ColourValue(1.0f, 218.0f/255.0f, 185.0f/255.0f));
	const NamedColour PresetColour::Peru("Peru", ColourValue(205.0f/255.0f, 133.0f/255.0f, 63.0f/255.0f));
	const NamedColour PresetColour::Pink("Pink", ColourValue(1.0f, 192.0f/255.0f, 203.0f/255.0f));
	const NamedColour PresetColour::Plum("Plum", ColourValue(221.0f/255.0f, 160.0f/255.0f, 221.0f/255.0f));
	const NamedColour PresetColour::PowderBlue("PowderBlue", ColourValue(176.0f/255.0f, 224.0f/255.0f, 230.0f/255.0f));
	const NamedColour PresetColour::Purple("Purple", ColourValue(128.0f/255.0f, 0.0f, 128.0f/255.0f));
	const NamedColour PresetColour::Red("Red", ColourValue(1.0f, 0.0f, 0.0f));
	const NamedColour PresetColour::RosyBrown("RosyBrown", ColourValue(188.0f/255.0f, 143.0f/255.0f, 143.0f/255.0f));
	const NamedColour PresetColour::RoyalBlue("RoyalBlue", ColourValue(65.0f/255.0f, 105.0f/255.0f, 225.0f/255.0f));
	const NamedColour PresetColour::SaddleBrown("SaddleBrown", ColourValue(139.0f/255.0f, 69.0f/255.0f, 19.0f/255.0f));
	const NamedColour PresetColour::Salmon("Salmon", ColourValue(250.0f/255.0f, 128.0f/255.0f, 114.0f/255.0f));
	const NamedColour PresetColour::SandyBrown("SandyBrown", ColourValue(244.0f/255.0f, 164.0f/255.0f, 96.0f/255.0f));
	const NamedColour PresetColour::SeaGreen("SeaGreen", ColourValue(46.0f/255.0f, 139.0f/255.0f, 87.0f/255.0f));
	const NamedColour PresetColour::SeaShell("SeaShell", ColourValue(1.0f, 245.0f/255.0f, 238.0f/255.0f));
	const NamedColour PresetColour::Sienna("Sienna", ColourValue(160.0f/255.0f, 82.0f/255.0f, 45.0f/255.0f));
	const NamedColour PresetColour::Silver("Silver", ColourValue(192.0f/255.0f, 192.0f/255.0f, 192.0f/255.0f));
	const NamedColour PresetColour::SkyBlue("SkyBlue", ColourValue(135.0f/255.0f, 206.0f/255.0f, 235.0f/255.0f));
	const NamedColour PresetColour::SlateBlue("SlateBlue", ColourValue(106.0f/255.0f, 90.0f/255.0f, 205.0f/255.0f));
	const NamedColour PresetColour::SlateGray("SlateGray", ColourValue(112.0f/255.0f, 128.0f/255.0f, 144.0f/255.0f));
	const NamedColour PresetColour::Snow("Snow", ColourValue(1.0f, 250.0f/255.0f, 250.0f/255.0f));
	const NamedColour PresetColour::SpringGreen("SpringGreen", ColourValue(0.0f, 1.0f, 127.0f/255.0f));
	const NamedColour PresetColour::SteelBlue("SteelBlue", ColourValue(70.0f/255.0f, 130.0f/255.0f, 180.0f/255.0f));
	const NamedColour PresetColour::Tan("Tan", ColourValue(210.0f/255.0f, 180.0f/255.0f, 140.0f/255.0f));
	const NamedColour PresetColour::Teal("Teal", ColourValue(0.0f, 128.0f/255.0f, 128.0f/255.0f));
	const NamedColour PresetColour::Thistle("Thistle", ColourValue(216.0f/255.0f, 191.0f/255.0f, 216.0f/255.0f));
	const NamedColour PresetColour::Tomato("Tomato", ColourValue(1.0f, 99.0f/255.0f, 71.0f/255.0f));
	const NamedColour PresetColour::TransparentBlack("TransparentBlack", ColourValue(0.0f, 0.0f, 0.0f, 0.0f));
	const NamedColour PresetColour::TransparentWhite("TransparentWhite", ColourValue(1.0f, 1.0f, 1.0f, 0.0f));
	const NamedColour PresetColour::Turquoise("Turquoise", ColourValue(64.0f/255.0f, 224.0f/255.0f, 208.0f/255.0f));
	const NamedColour PresetColour::Violet("Violet", ColourValue(238.0f/255.0f, 130.0f/255.0f, 238.0f/255.0f));
	const NamedColour PresetColour::Wheat("Wheat", ColourValue(245.0f/255.0f, 222.0f/255.0f, 179.0f/255.0f));
	const NamedColour PresetColour::White("White", ColourValue(1.0f, 1.0f, 1.0f));
	const NamedColour PresetColour::WhiteSmoke("WhiteSmoke", ColourValue(245.0f/255.0f, 245.0f/255.0f, 245.0f/255.0f));
	const NamedColour PresetColour::Yellow("Yellow", ColourValue(1.0f, 255.0f/255.0f, 0.0f));
	const NamedColour PresetColour::YellowGreen("YellowGreen", ColourValue(154.0f/255.0f, 205.0f/255.0f, 50.0f/255.0f));
	//---------------------------------------------------------------------
	PresetColour::ColourMap PresetColour::sColours = CreateColourMap
		(AliceBlue)
		(AntiqueWhite)
		(Aqua)
		(Aquamarine)
		(Azure)
		(Beige)
		(Bisque)
		(Black)
		(BlanchedAlmond)
		(Blue)
		(BlueViolet)
		(Brown)
		(BurlyWood)
		(CadetBlue)
		(Chartreuse)
		(Chocolate)
		(Coral)
		(CornflowerBlue)
		(Cornsilk)
		(Crimson)
		(Cyan)
		(DarkBlue)
		(DarkCyan)
		(DarkGoldenrod)
		(DarkGray)
		(DarkGreen)
		(DarkKhaki)
		(DarkMagenta)
		(DarkOliveGreen)
		(DarkOrange)
		(DarkOrchid)
		(DarkRed)
		(DarkSalmon)
		(DarkSeaGreen)
		(DarkSlateBlue)
		(DarkSlateGray)
		(DarkTurquoise)
		(DarkViolet)
		(DeepPink)
		(DeepSkyBlue)
		(DimGray)
		(DodgerBlue)
		(Firebrick)
		(FloralWhite)
		(ForestGreen)
		(Fuchsia)
		(Gainsboro)
		(GhostWhite)
		(Gold)
		(Goldenrod)
		(Gray)
		(Green)
		(GreenYellow)
		(Honeydew)
		(HotPink)
		(IndianRed)
		(Indigo)
		(Ivory)
		(Khaki)
		(Lavender)
		(LavenderBlush)
		(LawnGreen)
		(LemonChiffon)
		(LightBlue)
		(LightCoral)
		(LightCyan)
		(LightGoldenrodYellow)
		(LightGray)
		(LightGreen)
		(LightPink)
		(LightSalmon)
		(LightSeaGreen)
		(LightSkyBlue)
		(LightSlateGray)
		(LightSteelBlue)
		(LightYellow)
		(Lime)
		(LimeGreen)
		(Linen)
		(Magenta)
		(Maroon)
		(MediumAquamarine)
		(MediumBlue)
		(MediumOrchid)
		(MediumPurple)
		(MediumSeaGreen)
		(MediumSlateBlue)
		(MediumSpringGreen)
		(MediumTurquoise)
		(MediumVioletRed)
		(MidnightBlue)
		(MintCream)
		(MistyRose)
		(Moccasin)
		(NavajoWhite)
		(Navy)
		(OldLace)
		(Olive)
		(OliveDrab)
		(Orange)
		(OrangeRed)
		(Orchid)
		(PaleGoldenrod)
		(PaleGreen)
		(PaleTurquoise)
		(PaleVioletRed)
		(PapayaWhip)
		(PeachPuff)
		(Peru)
		(Pink)
		(Plum)
		(PowderBlue)
		(Purple)
		(Red)
		(RosyBrown)
		(RoyalBlue)
		(SaddleBrown)
		(Salmon)
		(SandyBrown)
		(SeaGreen)
		(SeaShell)
		(Sienna)
		(Silver)
		(SkyBlue)
		(SlateBlue)
		(SlateGray)
		(Snow)
		(SpringGreen)
		(SteelBlue)
		(Tan)
		(Teal)
		(Thistle)
		(Tomato)
		(TransparentBlack)
		(TransparentWhite)
		(Turquoise)
		(Violet)
		(Wheat)
		(White)
		(WhiteSmoke)
		(Yellow)
		(YellowGreen);
	//---------------------------------------------------------------------
	PresetColour::PresetColour(void)
	{
	}
	//---------------------------------------------------------------------
	PresetColour::~PresetColour(void)
	{
	}
	//---------------------------------------------------------------------
	inline void PresetColour::addColour( const NamedColour& colour )
	{
		sColours[colour.getName()] = &colour;
	}
	//---------------------------------------------------------------------
	size_t PresetColour::GetCount()
	{
		return sColours.size();
	}
	//---------------------------------------------------------------------
	bool PresetColour::HasColour( const std::string& name )
	{
		return sColours.count(name) != 0;
	}
	//---------------------------------------------------------------------
	NamedColour PresetColour::GetColour( const std::string& name )
	{
		if (sColours.count(name))
		{
			return *sColours[name];
		}
		std::stringstream ss;
		ss<<"Could not find preset colour '"<<name<<"'.";
		throw std::exception(ss.str().c_str());
	}
}


