#include <OpenRO/RoBackground.h>
#include <OpenRO/RoApplication.h>
#include <OpenRO/RoResourceLoader.h>
#include <Ogre/OgreIteratorWrapper.h>
#include <Ogre/OgreMaterialManager.h>

namespace ro
{
	Background::Background(void)
		:mRect(true)
		,mSceneNode(0)
		,mMaterial(0)
		,mCurrentImageIndex(0)
	{
	}

	Background::~Background(void)
	{
		mMaterial = 0;
		if (mSceneNode)
		{
			Application::GetInstance().getSceneManager().destroySceneNode(mSceneNode);
		}
	}

	void Background::init()
	{
		if (mImageArray.empty())
		{
			StringArray images = ResourceLoader::GetInstance().queryFiles("data\\\\texture\\\\.*\\\\loading[0-9]+\\.jpg", false);
			Ogre::VectorIterator<StringArray> image_file_itr(images);
			mImageArray.reserve(images.size());
			while (image_file_itr.hasMoreElements())
			{
				String file_name = image_file_itr.getNext();
				ImagePtr image = Image::Load(file_name);
				if (!image.isNull())
				{
					mImageArray.push_back(image);
				}
			}
		}
		if (mSceneNode == 0)
		{
			Ogre::MaterialPtr bgMaterial = Ogre::MaterialManager::getSingleton().create("OpenRO/Background", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
			mMaterial = bgMaterial.getPointer();
			Ogre::Technique* tech_0 = mMaterial->getTechnique(0);
			Ogre::Pass* pass_0 = tech_0->getPass(0);
			pass_0->setLightingEnabled(false);
			pass_0->setDepthCheckEnabled(false);
			pass_0->setDepthWriteEnabled(false);
			mTexture = pass_0->createTextureUnitState();
			mTexture->setTextureName(mImageArray[mCurrentImageIndex]->getName());
			mRect.setCorners(-1, 1, 1, -1);
			mRect.setBoundingBox(Ogre::AxisAlignedBox::BOX_INFINITE);
			mRect.setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
			mRect.setMaterial(mMaterial->getName());
			mSceneNode = Application::GetInstance().getSceneManager().getRootSceneNode()->createChildSceneNode();
			mSceneNode->attachObject(&mRect);
		}
	}

	void Background::setNext()
	{
		++mCurrentImageIndex;
		
		if (mImageArray.size() == mCurrentImageIndex)
			mCurrentImageIndex = 0;

		mTexture->setTextureName(mImageArray[mCurrentImageIndex]->getName());
	}

	void Background::setImageIndex( size_t imageIndex )
	{
		if (mImageArray.empty())
		{
			LOG_CRITICAL("Background image list is empty. Have you included the correct list of GRF or have the images in the Data folder?");
			return;
		}

		if (imageIndex > mImageArray.size())
		{
			LOG_CRITICAL("Background image index out of range. Gracefully handling it to show the next image in the list.");
			setNext();
			return;
		}
		mCurrentImageIndex = imageIndex;
		mTexture->setTextureName(mImageArray[mCurrentImageIndex]->getName());
	}

	size_t Background::getImageCount() const
	{
		return mImageArray.size();
	}

	void Background::setPrevious()
	{
		mCurrentImageIndex = mCurrentImageIndex == 0 ? mImageArray.size()-1 : mCurrentImageIndex - 1;
		mTexture->setTextureName(mImageArray[mCurrentImageIndex]->getName());
	}

	void Background::setVisible( bool visible )
	{
		mSceneNode->setVisible(visible);
	}
}
