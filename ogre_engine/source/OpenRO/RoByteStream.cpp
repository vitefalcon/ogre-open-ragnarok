#include <OpenRO/RoByteStream.h>


namespace ro
{
	ByteWriteStream::ByteWriteStream(void)
		:mStream(std::ios::binary)
	{
	}


	ByteWriteStream::~ByteWriteStream(void)
	{
	}

	void ByteWriteStream::writeInt8( int8 val )
	{
		mStream.write(static_cast<const char*>(&val), 1);
	}

	void ByteWriteStream::writeUInt8( uint8 val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 1);
	}

	void ByteWriteStream::writeInt16( int16 val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 2);
	}

	void ByteWriteStream::writeUInt16( uint16 val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 2);
	}

	void ByteWriteStream::writeInt32( int32 val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 4);
	}

	void ByteWriteStream::writeUInt32( uint32 val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 4);
	}

	void ByteWriteStream::writeFloat( float val )
	{
		mStream.write(reinterpret_cast<const char*>(&val), 4);
	}

	void ByteWriteStream::writeFixedString( const std::string& val, size_t size )
	{
		char* cstr = new char[size];
		memcpy_s((void*)cstr, size, val.c_str(), size);
		mStream.write(reinterpret_cast<const char*>(cstr), size);
		delete cstr;
	}

	void ByteWriteStream::writeString( const std::string& val )
	{
		size_t val_size = val.length();
		char* cstr = new char[val_size + 1];
		cstr[val_size] = 0;
		memcpy_s((void*)cstr, val_size, val.c_str(), val_size);
		delete cstr;
	}

	void ByteWriteStream::getBuffer( byte** buffer, size_t& length )
	{
		mStream.seekg(0, std::ios::end);
		length = mStream.tellg();
		mStream.seekg(0, std::ios::beg);

		*buffer = new byte[length];
		mStream.read(reinterpret_cast<char*>(*buffer), length);
	}
}
