#include <OpenRO/RoPalette.h>

namespace ro
{
	Palette::Palette(const PAL* pal)
	{
		for (short i = 0; i <= 255; ++i)
		{
			mData[i] = pal->getColor(static_cast<byte>(i));
		}
	}

	Palette::~Palette(void)
	{
	}

	ro::PalettePtr Palette::Load( const PAL* pal )
	{
		assert(pal != 0 && pal->isValid());
		PalettePtr palette(RO_NEW(Palette)(pal));
		return palette;
	}

	const RGBA& Palette::at( byte index ) const
	{
		return mData[index];
	}

	const RGBA& Palette::operator[]( byte index ) const
	{
		return mData[index];
	}

	RGBA::RGBA()
		:r(0)
		,g(0)
		,b(0)
		,a(255)
	{
	}

	RGBA::RGBA( const PAL::Color& col )
		:r(col.r)
		,g(col.g)
		,b(col.b)
		,a(255)
	{
	}

	RGBA::RGBA( const SPR::Color& col )
		:r(col.r)
		,g(col.g)
		,b(col.b)
		,a(col.a)
	{
	}

	RGBA& RGBA::operator=( const PAL::Color& col )
	{
		r = col.r;
		g = col.g;
		b = col.b;
		return *this;
	}

	RGBA& RGBA::operator=( const SPR::Color& col )
	{
		r = col.r;
		g = col.g;
		b = col.b;
		a = col.a;
		return *this;
	}

	bool RGBA::operator==( const RGBA& rhs ) const
	{
		return r == rhs.r && b == rhs.b && g == rhs.g && a == rhs.a;
	}

	bool RGBA::operator!=( const RGBA& rhs ) const
	{
		return r != rhs.r || b != rhs.b || g != rhs.g && a != rhs.a;
	}
}
