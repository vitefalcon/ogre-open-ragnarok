#include <OpenRO/RoObject3DFactory.h>
#include <OpenRO/RoObject3D.h>
#include <OpenRO/RoApplication.h>
#include <OpenRO/RoResourceLoader.h>
#include <OpenRO/RoInputStream.h>

#include <ro/types/rsm.h>

namespace ro
{
	Object3DFactory* Singleton<Object3DFactory>::s_instance = 0;

	Object3DFactory::Object3DFactory(void)
	{
	}

	Object3DFactory::~Object3DFactory(void)
	{
	}

	Object3DPtr Object3DFactory::load( const std::string& object_file_name )
	{
		Ogre::MeshManager& meshManager = Ogre::MeshManager::getSingleton();
		if (mObjects.count(object_file_name))
		{
			return mObjects[object_file_name];
		}
		else
		{
			InputStreamPtr inputStream = ResourceLoader::GetInstance().loadInputStream(object_file_name);
			RSM rsm;
			rsm.readStream(*inputStream);
			inputStream.setNull();
			if(rsm.isValid())
			{
				Ogre::SceneManager& sceneManager = Application::GetInstance().getSceneManager();
				Object3DPtr object(RO_NEW(Object3D)(object_file_name, sceneManager.getRootSceneNode()->createChildSceneNode()));
				object->mShiftHandedness = true;
				object->fromRsm(rsm);
				mObjects[object_file_name] = object;
				return object;
			}
		}
		return Object3D::Empty;
	}

	void Object3DFactory::unload( const std::string& object_file_name )
	{
		if (mObjects.count(object_file_name))
		{
			mObjects.erase(object_file_name);
		}
	}
}
