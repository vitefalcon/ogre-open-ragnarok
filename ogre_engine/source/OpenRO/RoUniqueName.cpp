#include <OpenRO/RoUniqueName.h>

namespace ro
{
	UniqueName::TypeNameMap UniqueName::sTypeName;
	UniqueName::CountMap UniqueName::sCount;

	UniqueName::UniqueName(void)
	{
	}

	UniqueName::~UniqueName(void)
	{
	}

	String UniqueName::Next( const std::type_info* type )
	{
		String type_name;
		if (sTypeName.count(type) == 0)
		{
			type_name = type->name();
			type_name = type_name.substr(type_name.find(' ')+1);
			sTypeName[type] = type_name;
		}
		else
		{
			type_name = sTypeName[type];
		}
		return Next(type_name);
	}

	Ogre::String UniqueName::Next( const std::string& name )
	{
		size_t count = 0;

		if (sCount.count(name))
			count = sCount[name];

		sCount[name] = count + 1;
		StringStream ss;
		ss<<name<<"["<<count<<"]";
		return ss.str();
	}
}