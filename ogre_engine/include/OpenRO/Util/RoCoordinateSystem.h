#pragma once
#ifndef __RoCoordinateSystem_H__
#define __RoCoordinateSystem_H__

#include <Ogre/OgreVector3.h>
#include <Ogre/OgreQuaternion.h>
#include <Ogre/OgreMatrix3.h>
#include <Ogre/OgreMatrix4.h>

namespace ro
{
	enum DimOps
	{
		DIMOP_NONE				= 0x0000,
		INVERT_XDIM				= 0x0001<<0,
		INVERT_YDIM				= 0x0001<<1,
		INVERT_ZDIM				= 0x0001<<2,
		INVERT_OP_MASK			= 0x0007,
		INVERT_SCALE_X			= 0x0001<<3,
		INVERT_SCALE_Y			= 0x0001<<4,
		INVERT_SCALE_Z			= 0x0001<<5,
		INVERT_SCALE_OP_MASK	= 0x0038,
		SWAP_XY					= 0x0001<<6,
		SWAP_YZ					= 0x0002<<6,
		SWAP_ZX					= 0x0003<<6,
		SWAP_OP_MASK			= 0x00C0
	};
	class CoordSys
	{
	public:
		static Ogre::Vector3 ConvertPos(const Ogre::Vector3& pos, unsigned short dimOps = INVERT_ZDIM)
		{
			if (dimOps == DIMOP_NONE)
				return pos;
			Ogre::Vector3 result = pos;
			unsigned short dimOp = dimOps & INVERT_OP_MASK;
			if (dimOp)
			{
				if (dimOps & INVERT_ZDIM)
					result.z = -result.z;
				if (dimOps & INVERT_YDIM)
					result.y = -result.y;
				if (dimOps & INVERT_XDIM)
					result.x = -result.x;
			}
			dimOp = dimOps & SWAP_OP_MASK;
			switch (dimOp)
			{
			case SWAP_XY:
				std::swap(result.x, result.y);
				break;
			case SWAP_YZ:
				std::swap(result.y, result.z);
				break;
			case SWAP_ZX:
				std::swap(result.z, result.x);
				break;
			default:
				break;
			}
			return result;
		}

		static Ogre::Quaternion ConvertRot(const Ogre::Radian& angle, const Ogre::Vector3& axis, unsigned short dimOps = INVERT_ZDIM)
		{
			if (angle.valueRadians() == 0.0f)
				return Ogre::Quaternion::IDENTITY;

			Ogre::Quaternion RHRot(angle, Ogre::Vector3(axis.x, axis.y, axis.z));
			if (dimOps == DIMOP_NONE)
				return RHRot;

			Ogre::Matrix3 RHRotMat;
			RHRot.ToRotationMatrix(RHRotMat);
			ConvertRotMatrix(RHRotMat, dimOps);
			RHRot.FromRotationMatrix(RHRotMat);
			return RHRot;
		}

		static Ogre::Quaternion ConvertRot(const Ogre::Quaternion& rhrot, unsigned short dimOps = INVERT_ZDIM, bool negateAngle = false)
		{
			if (dimOps == DIMOP_NONE && !negateAngle)
				return rhrot;

			Ogre::Matrix3 RHRotMat;
			if (negateAngle)
			{
				Ogre::Radian angle;
				Ogre::Vector3 axis;
				rhrot.ToAngleAxis(angle, axis);

				if (angle.valueRadians() == 0.0f)
					return Ogre::Quaternion::IDENTITY;

				RHRotMat.FromAxisAngle(axis, -angle);
			}
			else
			{
				rhrot.ToRotationMatrix(RHRotMat);
			}
			ConvertRotMatrix(RHRotMat, dimOps);
			return Ogre::Quaternion(RHRotMat);
		}

		static void ConvertTransformMatrix(Ogre::Matrix4& m, unsigned short dimOps = INVERT_ZDIM)
		{
			if (dimOps == DIMOP_NONE)
				return;
			unsigned short invertOp = dimOps & INVERT_OP_MASK;
			unsigned short invertScaleOp = dimOps & INVERT_SCALE_OP_MASK;
			if (invertOp)
			{
				if (invertOp & INVERT_ZDIM)
				{
					m[0][2] = -m[0][2];
					m[1][2] = -m[1][2];
					m[2][0] = -m[2][0];
					m[2][1] = -m[2][1];
					m[2][3] = -m[2][3];
				}
				if (invertOp & INVERT_YDIM)
				{
					m[0][1] = -m[0][1];
					m[1][2] = -m[1][2];
					m[1][0] = -m[1][0];
					m[2][1] = -m[2][1];
					m[1][3] = -m[1][3];
				}
				if (invertOp & INVERT_XDIM)
				{
					m[0][1] = -m[0][1];
					m[0][2] = -m[0][2];
					m[1][0] = -m[1][0];
					m[2][0] = -m[2][0];
					m[0][3] = -m[0][3];
				}
			}
			if (invertScaleOp)
			{
				if (invertScaleOp & INVERT_SCALE_X)
				{
					m[0][0] = -m[0][0];
				}
				if (invertScaleOp & INVERT_SCALE_Y)
				{
					m[1][1] = -m[1][1];
				}
				if (invertScaleOp & INVERT_SCALE_Z)
				{
					m[2][2] = -m[2][2];
				}
			}
		}

		static void ConvertRotMatrix(Ogre::Matrix3& m, unsigned short invertDims = INVERT_ZDIM)
		{
			if (invertDims == DIMOP_NONE)
				return;
			if (invertDims & INVERT_ZDIM)
			{
				m[0][2] = -m[0][2];
				m[1][2] = -m[1][2];
				m[2][0] = -m[2][0];
				m[2][1] = -m[2][1];
			}
			if (invertDims & INVERT_YDIM)
			{
				m[0][1] = -m[0][1];
				m[1][2] = -m[1][2];
				m[1][0] = -m[1][0];
				m[2][1] = -m[2][1];
			}
			if (invertDims & INVERT_XDIM)
			{
				m[0][1] = -m[0][1];
				m[0][2] = -m[0][2];
				m[1][0] = -m[1][0];
				m[2][0] = -m[2][0];
			}
		}
	private:
		CoordSys();
		~CoordSys();
	};
}

#endif // __RoCoordinateSystem_H__
