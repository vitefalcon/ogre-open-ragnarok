#pragma once
#ifndef __PRESETCOLOUR_H__
#define __PRESETCOLOUR_H__

#include <map>
#include <string>
#include <Ogre/OgreColourValue.h>
#include <Ogre/OgreIteratorWrappers.h>
#include <OpenRO/Util/RoNamedValue.h>

namespace ro
{
	typedef NamedValue<Ogre::ColourValue> NamedColour;

	class PresetColour
	{
	public:
		typedef std::map<std::string, const NamedColour*> ColourMap;
		typedef Ogre::ConstMapIterator<ColourMap> ColourIterator;
		typedef ColourMap::iterator iterator;
		typedef ColourMap::const_iterator const_iterator;

		static const NamedColour AliceBlue;
		static const NamedColour AntiqueWhite;
		static const NamedColour Aqua;
		static const NamedColour Aquamarine;
		static const NamedColour Azure;
		static const NamedColour Beige;
		static const NamedColour Bisque;
		static const NamedColour Black;
		static const NamedColour BlanchedAlmond;
		static const NamedColour Blue;
		static const NamedColour BlueViolet;
		static const NamedColour Brown;
		static const NamedColour BurlyWood;
		static const NamedColour CadetBlue;
		static const NamedColour Chartreuse;
		static const NamedColour Chocolate;
		static const NamedColour Coral;
		static const NamedColour CornflowerBlue;
		static const NamedColour Cornsilk;
		static const NamedColour Crimson;
		static const NamedColour Cyan;
		static const NamedColour DarkBlue;
		static const NamedColour DarkCyan;
		static const NamedColour DarkGoldenrod;
		static const NamedColour DarkGray;
		static const NamedColour DarkGreen;
		static const NamedColour DarkKhaki;
		static const NamedColour DarkMagenta;
		static const NamedColour DarkOliveGreen;
		static const NamedColour DarkOrange;
		static const NamedColour DarkOrchid;
		static const NamedColour DarkRed;
		static const NamedColour DarkSalmon;
		static const NamedColour DarkSeaGreen;
		static const NamedColour DarkSlateBlue;
		static const NamedColour DarkSlateGray;
		static const NamedColour DarkTurquoise;
		static const NamedColour DarkViolet;
		static const NamedColour DeepPink;
		static const NamedColour DeepSkyBlue;
		static const NamedColour DimGray;
		static const NamedColour DodgerBlue;
		static const NamedColour Firebrick;
		static const NamedColour FloralWhite;
		static const NamedColour ForestGreen;
		static const NamedColour Fuchsia;
		static const NamedColour Gainsboro;
		static const NamedColour GhostWhite;
		static const NamedColour Gold;
		static const NamedColour Goldenrod;
		static const NamedColour Gray;
		static const NamedColour Green;
		static const NamedColour GreenYellow;
		static const NamedColour Honeydew;
		static const NamedColour HotPink;
		static const NamedColour IndianRed;
		static const NamedColour Indigo;
		static const NamedColour Ivory;
		static const NamedColour Khaki;
		static const NamedColour Lavender;
		static const NamedColour LavenderBlush;
		static const NamedColour LawnGreen;
		static const NamedColour LemonChiffon;
		static const NamedColour LightBlue;
		static const NamedColour LightCoral;
		static const NamedColour LightCyan;
		static const NamedColour LightGoldenrodYellow;
		static const NamedColour LightGray;
		static const NamedColour LightGreen;
		static const NamedColour LightPink;
		static const NamedColour LightSalmon;
		static const NamedColour LightSeaGreen;
		static const NamedColour LightSkyBlue;
		static const NamedColour LightSlateGray;
		static const NamedColour LightSteelBlue;
		static const NamedColour LightYellow;
		static const NamedColour Lime;
		static const NamedColour LimeGreen;
		static const NamedColour Linen;
		static const NamedColour Magenta;
		static const NamedColour Maroon;
		static const NamedColour MediumAquamarine;
		static const NamedColour MediumBlue;
		static const NamedColour MediumOrchid;
		static const NamedColour MediumPurple;
		static const NamedColour MediumSeaGreen;
		static const NamedColour MediumSlateBlue;
		static const NamedColour MediumSpringGreen;
		static const NamedColour MediumTurquoise;
		static const NamedColour MediumVioletRed;
		static const NamedColour MidnightBlue;
		static const NamedColour MintCream;
		static const NamedColour MistyRose;
		static const NamedColour Moccasin;
		static const NamedColour NavajoWhite;
		static const NamedColour Navy;
		static const NamedColour OldLace;
		static const NamedColour Olive;
		static const NamedColour OliveDrab;
		static const NamedColour Orange;
		static const NamedColour OrangeRed;
		static const NamedColour Orchid;
		static const NamedColour PaleGoldenrod;
		static const NamedColour PaleGreen;
		static const NamedColour PaleTurquoise;
		static const NamedColour PaleVioletRed;
		static const NamedColour PapayaWhip;
		static const NamedColour PeachPuff;
		static const NamedColour Peru;
		static const NamedColour Pink;
		static const NamedColour Plum;
		static const NamedColour PowderBlue;
		static const NamedColour Purple;
		static const NamedColour Red;
		static const NamedColour RosyBrown;
		static const NamedColour RoyalBlue;
		static const NamedColour SaddleBrown;
		static const NamedColour Salmon;
		static const NamedColour SandyBrown;
		static const NamedColour SeaGreen;
		static const NamedColour SeaShell;
		static const NamedColour Sienna;
		static const NamedColour Silver;
		static const NamedColour SkyBlue;
		static const NamedColour SlateBlue;
		static const NamedColour SlateGray;
		static const NamedColour Snow;
		static const NamedColour SpringGreen;
		static const NamedColour SteelBlue;
		static const NamedColour Tan;
		static const NamedColour Teal;
		static const NamedColour Thistle;
		static const NamedColour Tomato;
		static const NamedColour TransparentBlack;
		static const NamedColour TransparentWhite;
		static const NamedColour Turquoise;
		static const NamedColour Violet;
		static const NamedColour Wheat;
		static const NamedColour White;
		static const NamedColour WhiteSmoke;
		static const NamedColour Yellow;
		static const NamedColour YellowGreen;

		static ColourIterator GetIterator();

		static NamedColour GetColour(const std::string& name);

		static bool HasColour(const std::string& name);

		static size_t GetCount();

		static iterator Begin(){return sColours.begin();}
		static iterator End(){return sColours.end();}
	private:
		PresetColour(void);
		~PresetColour(void);

		static void addColour(const NamedColour& colour);

		static ColourMap sColours;
	};
}

#endif // __PRESETCOLOUR_H__
