#pragma once
#ifndef __Flag_H__
#define __Flag_H__

#include <sstream>
#include <exception>

namespace ro
{
	template <typename NumericType>
	class Flag
	{
	public:
		typedef NumericType FlagType;
		static const unsigned short BIT_COUNT;
		static const Flag<NumericType> ZERO;
	private:
		NumericType m_flag;

		inline void validateBitIndex(size_t bit_index) const
		{
			if (bit_index >= BIT_COUNT)
			{
				std::stringstream ss;
				ss	<<"The bit index is out of range for this flag which holds a flag of size '"
					<<BIT_COUNT<<"'. The index you were trying to access was '"<<bit_index<<"'.";
				throw std::out_of_range(ss.str().c_str());
			}
		}
	public:

		Flag(void):m_flag(0){}
		Flag(const Flag<NumericType>& flag):m_flag(flag.m_flag){}
		explicit Flag(const NumericType& initial_value):m_flag(initial_value){}

		template <typename NumericType2>
		explicit Flag(const Flag<NumericType2>& flag):m_flag(flag.m_flag){}

		template <typename NumericType2>
		explicit Flag(const NumericType2& initial_value):m_flag((NumericType)initial_value){}

		~Flag(void){}

		const Flag<NumericType>& operator = (const Flag<NumericType>& rhs)
		{
			m_flag = rhs.m_flag;
			return *this;
		}

		template <typename NumericType2>
		const Flag<NumericType>& operator = (const Flag<NumericType2>& rhs)
		{
			m_flag = rhs.m_flag;
			return *this;
		}

		bool operator == (const Flag<NumericType>& rhs) const
		{
			// Return value optimisation.
			bool result = (m_flag == rhs.m_flag);
			return result;
		}

		template <typename NumericType2>
		bool operator == (const Flag<NumericType2>& rhs) const
		{
			// Return value optimisation.
			NumericType rhs_flag = (NumericType)rhs.m_flag;
			bool result = (m_flag == rhs_flag);
			return result;
		}

		bool operator != (const Flag<NumericType>& rhs) const
		{
			// Return value optimisation.
			bool result = (m_flag != rhs.m_flag);
			return result;
		}

		template <typename NumericType2>
		bool operator != (const Flag<NumericType2>& rhs) const
		{
			// Return value optimisation.
			NumericType rhs_flag = (NumericType)rhs.m_flag;
			bool result = (m_flag != rhs_flag);
			return result;
		}

		const Flag<NumericType>& operator += (const Flag<NumericType>& rhs)
		{
			m_flag |= rhs.m_flag;
			return *this;
		}

		template <typename NumericType2>
		const Flag<NumericType>& operator += (const Flag<NumericType2>& rhs)
		{
			NumericType rhs_flag = (NumericType)rhs.m_flag;
			m_flag |= rhs_flag;
			return *this;
		}

		Flag<NumericType> operator + (const Flag<NumericType>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp += rhs;
			return temp;
		}

		template <typename NumericType2>
		Flag<NumericType> operator + (const Flag<NumericType2>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp += rhs;
			return temp;
		}

		const Flag<NumericType>& operator |= (const Flag<NumericType>& rhs)
		{
			(*this) += rhs;
			return *this;
		}

		template <typename NumericType2>
		const Flag<NumericType>& operator |= (const Flag<NumericType2>& rhs)
		{
			(*this) += rhs;
			return *this;
		}

		Flag<NumericType> operator | (const Flag<NumericType>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp += rhs;
			return temp;
		}

		template <typename NumericType2>
		Flag<NumericType> operator | (const Flag<NumericType2>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp += rhs;
			return temp;
		}

		Flag<NumericType> operator ~ () const
		{
			Flag<NumericType> temp(~m_flag);
			return temp;
		}

		const Flag<NumericType>& operator -= (const Flag<NumericType>& rhs)
		{
			m_flag &= ~(rhs.m_flag);
			return *this;
		}

		template <typename NumericType2>
		const Flag<NumericType>& operator -= (const Flag<NumericType2>& rhs)
		{
			NumericType rhs_flag = (NumericType)rhs.m_flag;
			m_flag &= ~(rhs_flag);
			return *this;
		}

		Flag<NumericType> operator - (const Flag<NumericType>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp -= rhs;
			return temp;
		}

		template <typename NumericType2>
		Flag<NumericType> operator - (const Flag<NumericType2>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp -= rhs;
			return temp;
		}

		const Flag<NumericType>& operator &= (const Flag<NumericType>& rhs)
		{
			m_flag &= rhs.m_flag;
			return *this;
		}

		template <typename NumericType2>
		const Flag<NumericType>& operator &= (const Flag<NumericType2>& rhs)
		{
			NumericType rhs_flag = (NumericType)rhs.m_flag;
			m_flag &= rhs_flag;
			return *this;
		}

		Flag<NumericType> operator & (const Flag<NumericType>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp &= rhs;
			return temp;
		}

		template <typename NumericType2>
		Flag<NumericType> operator & (const Flag<NumericType2>& rhs) const
		{
			Flag<NumericType> temp(*this);
			temp &= rhs;
			return temp;
		}

		bool operator [] (size_t bit_index) const
		{
			validateBitIndex(bit_index);
			NumericType flag = 1 << bit_index;
			bool result = ((m_flag & flag) != 0);
			return result;
		}

		bool operator () (const Flag<NumericType>& rhs) const
		{
			bool result = ((m_flag & rhs.m_flag) == rhs.m_flag);
			return result;
		}

		bool hasBitEnabled(size_t bit_index) const
		{
			return (*this)[bit_index];
		}

		void setBit(size_t bit_index, bool enabled)
		{
			validateBitIndex(bit_index);
			Flag<NumericType> flag(1 << bit_index);
			if (enabled)
			{
				(*this) += flag;
			}else{
				(*this) -= flag;
			}
		}

		void flipBits()
		{
			Flag<NumericType> temp(*this);
			(*this) = ~temp;
		}

		bool hasFlag(const Flag<NumericType>& flag) const
		{
			return (*this)(flag);
		}

		void setFlag(const Flag<NumericType>& flag, bool enabled)
		{
			if (enabled)
			{
				(*this) += flag;
			}else{
				(*this) -= flag;
			}
		}

		void setFlag(const NumericType& raw_flag, bool enabled)
		{
			Flag<NumericType> flag(raw_flag);
			if (enabled)
			{
				(*this) += flag;
			}else{
				(*this) -= flag;
			}
		}

		bool empty() const
		{
			bool result = m_flag == 0;
			return result;
		}

		bool notEmpty() const
		{
			bool result = m_flag != 0;
			return result;
		}

		//explicit operator NumericType() const
		//{
		//	return m_flag;
		//}

		//template <typename NumericType2>
		//explicit operator NumericType2() const
		//{
		//	return (NumericType2)m_flag;
		//}

		NumericType& value()
		{
			return m_flag;
		}

		const NumericType& value() const
		{
			return m_flag;
		}
	};

	template <typename NumericType>
	const unsigned short Flag<NumericType>::BIT_COUNT = sizeof(NumericType) * 8;

	template<typename NumericType>
	const Flag<NumericType> Flag<NumericType>::ZERO;
}

#endif // __Flag_H__
