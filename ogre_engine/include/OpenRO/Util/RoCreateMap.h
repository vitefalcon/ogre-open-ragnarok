#pragma once
#ifndef __CREATEMAP_H__
#define __CREATEMAP_H__

#include <map>

namespace ro
{
	template <typename Key, typename Value>
	class CreateMap
	{
	public:
		typedef Key _Key;
		typedef Value _Value;

		typedef std::map<_Key, _Value> MapType;
	private:
		MapType m_map;

	public:
		CreateMap(_Key key, _Value val)
		{
			m_map[key] = val;
		}

		~CreateMap(void){}

		CreateMap& operator() (_Key key, _Value val)
		{
			m_map[key] = val;
			return *this;
		}

		operator std::map<Key, Value>()
		{
			return m_map;
		}
	};
}

#endif // __CREATEMAP_H__
