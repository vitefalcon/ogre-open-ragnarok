#pragma once
#ifndef __NamedValue_H__
#define __NamedValue_H__

#include <string>

namespace ro
{
	template <typename T>
	class NamedValue
	{
	public:

		NamedValue(const std::string& name, const T& val)
			:mName(name)
			,mValue(val)
		{
		}

		NamedValue(const NamedValue<T>& other)
			:mName(other.mName)
			,mValue(other.mValue)
		{
		}

		virtual ~NamedValue(void)
		{
		}

		operator T() const
		{
			return mValue;
		}

		const std::string& getName() const
		{
			return mName;
		}

		const T& getValue() const
		{
			return mValue;
		}
	protected:
		const std::string mName;
		const T mValue;
	};
}

#endif // __NamedValue_H__
