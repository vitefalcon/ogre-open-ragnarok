#pragma once
#ifndef __RoRsmMeshBuilder_H__
#define __RoRsmMeshBuilder_H__

#include <OpenRO/RoPrerequisites.h>

#include <ro/ro_arr.h>
#include <ro/types/rsm.h>

#include <Ogre/OgreAxisAlignedBox.h>
#include <Ogre/OgreColourValue.h>
#include <Ogre/OgreVector2.h>
#include <Ogre/OgreVector3.h>
#include <Ogre/OgreIteratorWrapper.h>

namespace ro
{
	namespace Util
	{
		struct RsmVertexIndex
		{
			ushort vertexIndex;
			ushort tvertexIndex;

			RsmVertexIndex(ushort vertexIdx, ushort tvertexIdx);

			bool operator < (const RsmVertexIndex& rhs) const;
			bool operator == (const RsmVertexIndex& rhs) const;
			bool operator != (const RsmVertexIndex& rhs) const;
		};

#		pragma pack(push,1)
		struct RsmVertex
		{
			Ogre::Vector3 p;
			Ogre::Vector3 n;
			Ogre::ABGR c;
			Ogre::Vector2 t;

			RsmVertex();

			static const RsmVertex Zero;
		};
#		pragma pack(pop)

		typedef std::deque<RsmVertexIndex> RsmVertexIndexDeque;
		typedef std::vector<RsmVertex> VertexList;

		class RsmSubMesh
		{
		public:
			RsmSubMesh();

			void addFace(const RSM::Face& face);

			Ogre::AxisAlignedBox fillData(Ogre::VertexData& vertexData, Ogre::IndexData& indexData, const RSM::Node& node);

			Ogre::AxisAlignedBox buildVertexList(const RSM::Node& node, const Ogre::Matrix4& transformMatrix, const Ogre::Vector3& offset);
		private:
#		pragma pack(push, 1)
			struct Triangle
			{
				ushort vertexIndices[3];

				Triangle();
				Triangle(const Triangle& rhs);
				Triangle& operator = (const Triangle& rhs);
			};
#		pragma pack(pop)
			std::vector<Triangle> mFaces;
			ushort addVertexIndex(const RsmVertexIndex& vIdx);
			typedef std::deque<ushort> IndexDeque;
			typedef std::vector<ushort> IndexArray;
			RsmVertexIndexDeque mVertexIndices;
			IndexDeque mIndexDeque;
			IndexArray mIndices;
			VertexList mVertices;
		};

		typedef Ogre::SharedPtr<RsmSubMesh> RsmSubMeshPtr;
		typedef std::map<int, RsmSubMeshPtr> SubMeshMap;
		typedef Ogre::ConstMapIterator<SubMeshMap> ConstSubMeshItr;

		class RsmMeshBuilder
		{
		public:
			typedef Ogre::ConstVectorIterator<VertexList> ConstVertexItr;

			RsmMeshBuilder(const RSM::Node& rsmNode);
			~RsmMeshBuilder();

			void build();

			void* getVertexPtr();

			ConstSubMeshItr getIterator() const;

			ConstVertexItr getVertexIterator() const;

			const Ogre::AxisAlignedBox& getBounds() const;

			static size_t GetVertexSizeInBytes();
		private:
			const RSM::Node &mRsmNode;
			SubMeshMap mSubMeshes;
			std::vector<RsmVertex> mVertices;
			Ogre::AxisAlignedBox mBounds;
		};

		void CreateRsmMesh(const std::string& subMeshName, const RSM::Node& node, const StringArray& materialNames);
		std::string CreateRsmObjectMaterial(const std::string& textureName);
	}
}

#endif // __RoRsmMeshBuilder_H__

