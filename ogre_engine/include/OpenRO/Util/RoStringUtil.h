#pragma once
#ifndef __RoStringUtil_H__
#define __RoStringUtil_H__

#include <string>
#include <ctype.h>

using std::string;
using std::wstring;

namespace ro
{
	class StringUtil
	{
	public:
		static string ToUtf8(const wstring& wstr);
		static wstring ToUtf16(const string& str);
		static string toLower(const string &s)
		{
			string result;
			result.reserve(s.length());
			for (unsigned int i = 0; i < s.length(); i++)
				result += tolower(s[i]);
			return result;
		};

	private:
		StringUtil();
		~StringUtil();
	};
}

#endif // __RoStringUtil_H__
