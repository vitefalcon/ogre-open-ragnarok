#pragma once
#ifndef __Object3D_H__
#define __Object3D_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoBaseObject.h>

namespace ro
{
	class RSM;

	class Object3D : public BaseObject
	{
	public:
		virtual ~Object3D(void);

		const String& getName() const;

		void setPosition(const Ogre::Vector3& pos);
		const Ogre::Vector3& getPosition() const;

		bool isAnimated() const;

		void setScale(const Ogre::Vector3& scale);
		const Ogre::Vector3& getScale() const;

		void setOrientation(const Ogre::Quaternion& orientation);
		const Ogre::Quaternion& getOrientation() const;

		void translate(const Ogre::Vector3& pos);
		void rotate(const Ogre::Radian& deltaAngle, const Ogre::Vector3& axis);
		void scale(const Ogre::Vector3& scale);

		Ogre::Vector3 getSize() const;

		static const Object3DPtr Empty;

		friend std::ostream& operator << (std::ostream& stream, const Object3D& obj);

	protected:
		friend class Object3DFactory;
		class SubObject;
		typedef Ogre::SharedPtr<SubObject> SubObjectPtr;
		typedef std::map<String, SubObjectPtr> SubOjbectMap;
		typedef std::map<String, size_t> RsmNameIndexMap;

		class SubObject : public BaseObject
		{
		public:
			SubObject(const std::string& name, Ogre::Entity* entity, Ogre::SceneNode* node, Ogre::SceneNode* animNode, Ogre::Animation* posAnim, Ogre::Animation* rotAnim);
			~SubObject();

			const std::string& getName() const;

			Ogre::SceneNode* getNode();

			Ogre::SceneNode* getAnimationNode();

			Ogre::Entity* getEntity();

			Ogre::Vector3 getSize() const;

			SubObjectPtr createChild(const String& subObjectName, RSM& rsm, size_t rsmNodeIndex, const StringArray& materialNames);

			std::ostream& print(std::ostream& stream, int intendation);

			static SubObjectPtr CreateSubObject(const String& subObjectName, Ogre::SceneNode* parentNode, RSM& rsm, size_t rsmNodeIndex, const StringArray& materialNames);
		private:
			const std::string	mName;
			SubOjbectMap		mChildren;
			Ogre::Entity*		mEntity;
			Ogre::SceneNode*	mNode;
			Ogre::SceneNode*	mAnimNode00;
			Ogre::Animation*	mPosAnimation00;
			Ogre::Animation*	mRotAnimation00;
		};

		Object3D(const std::string& name, Ogre::SceneNode* node);
		bool fromRsm(RSM& rsm);
		String genSubObjectName(const String& nodeName);
		void createSubObject(RSM& rsm, size_t rsmNodeIndex, RsmNameIndexMap& rsmNameIndexMap, const StringArray& materialNames);
		SubObjectPtr createSubObject(const String& subObjectName, Ogre::SceneNode* parentNode, RSM& rsm, size_t rsmNodeIndex, const StringArray& materialNames);

		const std::string	mName;
		Ogre::SceneNode*	mNode;
		bool				mShiftHandedness;
		unsigned int		mAnimationLength;
		String				mMainSubObject;
		SubOjbectMap		mSubObjects;
	};
}

#endif // __Object3D_H__
