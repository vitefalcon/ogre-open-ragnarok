#pragma once
#ifndef __Background_H__
#define __Background_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoBaseObject.h>
#include <OpenRO/RoImage.h>
#include <Ogre/OgreRectangle2D.h>
#include <Ogre/OgreMaterial.h>

namespace ro
{
	class Background : public BaseObject
	{
	public:
		Background(void);
		~Background(void);

		void init();

		void setImageIndex(size_t imageIndex);
		size_t getImageCount() const;

		void setNext();
		void setPrevious();

		void setVisible(bool visible);
	private:
		Ogre::SceneNode* mSceneNode;
		Ogre::Rectangle2D mRect;
		ImagePtr mImage;
		typedef std::vector<ImagePtr> ImageArray;
		ImageArray mImageArray;
		Ogre::Material* mMaterial;
		Ogre::TextureUnitState* mTexture;
		size_t mCurrentImageIndex;
	};
}

#endif // __Background_H__
