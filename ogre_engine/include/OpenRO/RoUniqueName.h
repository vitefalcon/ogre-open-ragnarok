#pragma once
#ifndef __UniqueName_H__
#define __UniqueName_H__

#include <OpenRO/RoPrerequisites.h>
#include <map>
#include <typeinfo>

namespace ro
{
	class UniqueName
	{
	public:
		template <typename T>
		static String Next();

		static String Next(const std::string& name);
	private:
		static String Next(const std::type_info* type);
		typedef std::map<const std::type_info*, std::string> TypeNameMap;
		typedef std::map<std::string, size_t> CountMap;
		static TypeNameMap sTypeName;
		static CountMap sCount;
		UniqueName(void);
		~UniqueName(void);
	};

	template <typename T>
	String UniqueName::Next()
	{
		return Next(&typeid(T));
	}
}

#endif // __UniqueName_H__
