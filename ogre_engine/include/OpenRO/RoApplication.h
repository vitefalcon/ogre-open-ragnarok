#pragma once
#ifndef __Application_H__
#define __Application_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoSingleton.h>
#include <OpenRO/RoMiniMap.h>
#include <OpenRO/RoSprite.h>
#include <OpenRO/RoObject3D.h>
#include <Ogre/Ogre.h>
#include <Ogre/OgreWindowEventUtilities.h>
#include <Ogre/OgreFrameListener.h>
#include <Ogre/OgreOverlay.h>
#include <OIS/OIS.h>

namespace ro
{
	class Application : public Singleton<Application>, public Ogre::WindowEventListener, public Ogre::FrameListener, public OIS::KeyListener, public OIS::MouseListener
	{
	public:
		Application();
		~Application(void);

		void run();

		Ogre::RenderWindow& getWindow();

		Ogre::SceneManager& getSceneManager();

		Ogre::Camera& getCamera();

		Ogre::Viewport& getViewport();

		Ogre::Vector2 getViewportSize() const;

		Ogre::Real getDisplayAspectRatio() const;

		SpriteFactory& getSpriteFactory() const;

		bool hasKeyboardModifierPressed() const;
	private:
		Application(const Application&);
		const Application& operator = (const Application &);

		/**
		@Remarks
			Window has moved position
		@param rw
			The RenderWindow which created this events
		*/
		virtual void windowMoved(Ogre::RenderWindow* rw);

		/**
		@Remarks
			Window has resized
		@param rw
			The RenderWindow which created this events
		*/
		virtual void windowResized(Ogre::RenderWindow* rw);

		/**
		@Remarks
			Window is closing (Only triggered if user pressed the [X] button)
		@param rw
			The RenderWindow which created this events
		@return True will close the window(default).
		*/
		virtual bool windowClosing(Ogre::RenderWindow* rw);

		/**
		@Remarks
			Window has been closed (Only triggered if user pressed the [X] button)
		@param rw
			The RenderWindow which created this events
		@note
			The window has not actually close yet when this event triggers. It's only closed after
			all windowClosed events are triggered. This allows apps to deinitialise properly if they
			have services that needs the window to exist when deinitialising.
		*/
		virtual void windowClosed(Ogre::RenderWindow* rw);

		/**
		@Remarks
			Window has lost/gained focus
		@param rw
			The RenderWindow which created this events
		*/
		virtual void windowFocusChange(Ogre::RenderWindow* rw);

        /** Called when a frame is about to begin rendering.
		@remarks
			This event happens before any render targets have begun updating. 
            @return
                True to go ahead, false to abort rendering and drop
                out of the rendering loop.
        */
        virtual bool frameStarted(const Ogre::FrameEvent& evt);
		
		/** Called after all render targets have had their rendering commands 
			issued, but before render windows have been asked to flip their 
			buffers over.
		@remarks
			The usefulness of this event comes from the fact that rendering 
			commands are queued for the GPU to process. These can take a little
			while to finish, and so while that is happening the CPU can be doing
			useful things. Once the request to 'flip buffers' happens, the thread
			requesting it will block until the GPU is ready, which can waste CPU
			cycles. Therefore, it is often a good idea to use this callback to 
			perform per-frame processing. Of course because the frame's rendering
			commands have already been issued, any changes you make will only
			take effect from the next frame, but in most cases that's not noticeable.
		@return
			True to continue rendering, false to drop out of the rendering loop.
		*/
		virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);

        /** Called just after a frame has been rendered.
		@remarks
			This event happens after all render targets have been fully updated
			and the buffers switched.
            @return
                True to continue with the next frame, false to drop
                out of the rendering loop.
        */
		virtual bool frameEnded(const Ogre::FrameEvent& evt);

		void initialiseInputs();
		void loadDataFiles();

		virtual bool keyPressed(const OIS::KeyEvent &arg);
		virtual bool keyReleased(const OIS::KeyEvent &arg);

		virtual bool mouseMoved( const OIS::MouseEvent &arg );
		virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
		virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

		bool init();

		static const String	PluginPath;

		Ogre::Root*			mRoot;
		Ogre::RenderWindow* mWindow;
		OIS::InputManager*	mInputManager;
		OIS::Keyboard*		mKeyboard;
		OIS::Mouse*			mMouse;
		ResourceLoader*		mResourceLoader;
		//FIXME: Debug purposes only
		StringArray			mRsmFiles;
		size_t				mRsmIndex;
		Object3DPtr			mObject3D;
		// ENDOFFIXME
		Object3DFactory*	mObject3DFactory;
		Ogre::SceneManager* mSceneManager;
		Ogre::Camera*		mCamera;
		Ogre::Viewport*		mViewport;
		Background*			mBackground;
		MiniMap*			mMiniMap;
		SpritePtr			mTestSprite;
		bool				mRenderFrames;
		Ogre::Real			mCurrentTime;

		int					mWindowWidth;
		int					mWindowHeight;
	};
}

#endif // __Application_H__
