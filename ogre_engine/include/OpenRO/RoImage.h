#pragma once
#ifndef __Image_H__
#define __Image_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoBaseObject.h>
#include <OpenRO/RoPalette.h>
#include <Ogre/OgreSharedPtr.h>
#include <Ogre/OgrePixelFormat.h>
#include "Ogre/OgreColourValue.h"

namespace ro
{
	class Image : public BaseObject
	{
	public:
		~Image(void);

		size_t getWidth() const;
		size_t getHeight() const;
		size_t getDepth() const;
		const std::string& getName() const;

		void setPalette(const Palette& pal, byte* palette_indices);

		void save(const String& fileName);

		static ImagePtr Load(const std::string& image_file_name, bool useColorKeyTransparency = false, const Ogre::ColourValue &keyCol = Ogre::ColourValue::ZERO, float threshold = 0.0f);

		static ImagePtr Create(const String& name, RGBA* data, ushort width, ushort height, int texture_usage = TextureImageUsage, Ogre::PixelFormat pixel_format = Ogre::PF_R8G8B8A8);

		static const int TextureImageUsage;
		static const int PaletteImageUsage;
	private:
		Image();

		static void ColorKeyTransparency(Ogre::Image &img, const Ogre::ColourValue &keyCol, float threshold = 0.0f);

		size_t mWidth;
		size_t mHeight;
		size_t mDepth;
		std::string mTextureName;
	};
}

#endif // __Image_H__
