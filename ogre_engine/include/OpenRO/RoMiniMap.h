#pragma once
#ifndef __MiniMap_H__
#define __MiniMap_H__

#include <OpenRO/RoBaseObject.h>
#include <Ogre/OgreOverlay.h>

namespace ro
{
	class MiniMap : public BaseObject
	{
	public:
		MiniMap(void);
		~MiniMap(void);

		void init();
		void setVisible(bool visible);
		bool getVisible() const;
		void destroy();
	private:
		Ogre::Overlay* mOverlay;
		Ogre::OverlayContainer* mImage;
	};
}

#endif // __MiniMap_H__
