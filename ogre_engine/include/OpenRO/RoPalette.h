#pragma once
#ifndef __Palette_H__
#define __Palette_H__

#include <OpenRO/RoBaseObject.h>
#include <ro/types/pal.h>
#include <ro/types/spr.h>
#include <Ogre/OgreSharedPtr.h>

namespace ro
{
	class Palette;
	typedef Ogre::SharedPtr<Palette> PalettePtr;

#pragma pack(push,1)
	struct RGBA
	{
		byte r;
		byte g;
		byte b;
		byte a;

		RGBA();
		RGBA(const PAL::Color& col);
		RGBA(const SPR::Color& col);

		RGBA& operator = (const PAL::Color& col);
		RGBA& operator = (const SPR::Color& col);

		bool operator == (const RGBA& rhs) const;
		bool operator != (const RGBA& rhs) const;
	};
#pragma pack(pop)

	class Palette : public BaseObject
	{
	public:

		~Palette(void);

		const RGBA& at(byte index) const;

		const RGBA& operator[](byte index) const;

		static PalettePtr Load(const PAL* pal);
	private:
		Palette(const PAL* pal);
		RGBA mData[256];
	};
}

#endif // __Palette_H__
