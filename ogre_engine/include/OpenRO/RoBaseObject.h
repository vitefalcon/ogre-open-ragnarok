#pragma once
#ifndef __BaseObject_H__
#define __BaseObject_H__


#include <OpenRO/RoPrerequisites.h>
#include <string>

#if __OPENRO_TRACK_MEMORY
#	include <typeinfo>
#	include <map>
#	include <sstream>
#	include <ctime>

#	define RO_NEW(_type) new (__FILE__, __FUNCTION__, __LINE__, typeid(_type)) _type
#	pragma warning(disable:4291)
#else
#	define RO_NEW(_type) new _type
#endif // __OPENRO_TRACK_MEMORY

//void* operator new(
//				   unsigned int size
//#ifdef _DEBUG
//				   ,const char* file
//				   ,const char* func
//				   ,int line
//				   ,const std::type_info& type
//#endif // _DEBUG
//				   );
//
//void* operator new[](
//					 unsigned int size
//#ifdef _DEBUG
//					 ,const char* file
//					 ,const char* func
//					 ,int line
//					 ,const std::type_info& type
//#endif // _DEBUG
//					 );
//
//void operator delete(void* mem);
//void operator delete[](void* mem);

namespace ro
{
	class BaseObject
	{
	public:
		virtual ~BaseObject(void);

		void* operator new(
			unsigned int size
#if __OPENRO_TRACK_MEMORY
			,const char* file
			,const char* func
			,int line
			,const std::type_info& type
#endif // __OPENRO_TRACK_MEMORY
			);

		void* operator new[](
			unsigned int size
#if __OPENRO_TRACK_MEMORY
			,const char* file
			,const char* func
			,int line
			,const std::type_info& type
#endif // __OPENRO_TRACK_MEMORY
			);

		void operator delete(void* mem);
		void operator delete[](void* mem);
	protected:
		BaseObject(void);

	private:
#if __OPENRO_TRACK_MEMORY
		friend class HeapLogCreator;

		struct HeapEntry
		{
			static size_t s_heapId;

			size_t id;
			size_t heapSize;
			std::string fileName;
			std::string functionName;
			int lineNumber;
			const std::type_info* typeInfo;
			bool isArray;

			HeapEntry();
			HeapEntry(size_t heap_size, const std::string& file_name, const std::string& func_name, const int _line, const std::type_info& info, bool is_array);
			HeapEntry(const HeapEntry& other);
			~HeapEntry();

			std::string className();

			const HeapEntry& operator = (const HeapEntry& rhs);

			bool operator == (const HeapEntry& rhs);
			bool operator != (const HeapEntry& rhs);

			bool operator < (const HeapEntry& rhs);
			bool operator <= (const HeapEntry& rhs);
			bool operator > (const HeapEntry& rhs);
			bool operator >= (const HeapEntry& rhs);
		};

		class HeapLogger
		{
		public:
			HeapLogger();
			~HeapLogger();

			static HeapLogger& GetInstance();
			static HeapLogger* GetInstancePtr();

			void logCreation(void* obj, HeapEntry entry);
			void logDeletion(void* obj);
		private:
			static HeapLogger* sInstance;

			typedef std::map<void*,HeapEntry> ObjectMap;
			ObjectMap mMap;
			size_t mTotalHeapSize;
			std::stringstream mStream;
			std::string mStartTimeStr;
		};

		static HeapLogger* sHeapLoggerInstance;

		static void CreateHeapLogger();
		static void DestroyHeapLogger();
#endif // __OPENRO_TRACK_MEMORY
	};

#if __OPENRO_TRACK_MEMORY
	class HeapLogCreator
	{
	public:
		HeapLogCreator();
		~HeapLogCreator();
	};
#endif // __OPENRO_TRACK_MEMORY
}

#endif // __BaseObject_H__
