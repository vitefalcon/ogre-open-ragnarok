#pragma once
#ifndef __RoInputStream_H__
#define __RoInputStream_H__

#if 0
#include <OpenRO/RoPrerequisites.h>
#include <istream>
#include <strstream>

namespace ro
{
	class InputStream :
		public std::istream
	{
	public:
		~InputStream(void);

		static InputStreamPtr Create(byte* bytes, size_t size);
	private:
		InputStream(std::strstreambuf* buffer, byte* bytes);
		std::strstreambuf* mBuffer;
		byte* mBytes;
	};
}
#endif

#endif // __RoInputStream_H__
