#pragma once
#ifndef __Sprite_H__
#define __Sprite_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoBaseObject.h>
#include <OpenRO/RoImage.h>
#include <OpenRO/RoPalette.h>
#include <Ogre/OgreSharedPtr.h>
#include <Ogre/OgreBillboardSet.h>
#include <Ogre/OgreBillboard.h>
#include <Ogre/OgreMaterial.h>
#include <ro/types/spr.h>
#include <vector>
#include <Gorilla/Gorilla.h>

namespace ro
{
	class Sprite;
	typedef Ogre::SharedPtr<Sprite> SpritePtr;

	class Sprite : public BaseObject
	{
	public:
		enum FrameType
		{
			FRAME_PALETTE,
			FRAME_RGBA
		};

		class Frame;
		typedef Ogre::SharedPtr<Frame> FramePtr;
		typedef std::vector<FramePtr> Frames;

		class Frame
		{
		protected:
			Frame(Sprite& parent, FrameType frame_type, size_t frame_index);
			const FrameType mType;
			SPR::Image mSprImage;
			ImagePtr mImage;
			Sprite& mParent;
			const size_t mFrameIndex;
			const String mName;
		public:
			virtual ~Frame();

			virtual void init(const SPR::Image& img) = 0;

			const String& getName() const;

			Sprite& getParent();
			const Sprite& getParent() const;

			ImagePtr getImage();

			FrameType getType() const;

			size_t getIndex() const;

			static FramePtr Create(uint index, Sprite& parent, PalettePtr palette, const SPR::Image& img);
		};
		class PaletteFrame : public Frame
		{
		public:
			PaletteFrame(Sprite& parent, size_t frame_index);
			~PaletteFrame();
			virtual void init(const SPR::Image& img);
			virtual void setPalette(const PalettePtr& palette);
			virtual PalettePtr getPalette() const;
		private:
			PalettePtr mPalette;
			byte* mPaletteIndex;
		};
		class RgbaFrame : public Frame
		{
		public:
			RgbaFrame(Sprite& parent, size_t frame_index);
			~RgbaFrame();
			virtual void init(const SPR::Image& img);
		};

		~Sprite(void);

		const String& getName() const;

		void setPosition(const Ogre::Vector3& position);
		const Ogre::Vector3& getPosition() const;

		void setFrame(uint frame_index);

		static SpritePtr Load(const String& file_name);
	private:
		friend class SpriteFactory;
		Sprite(const String& name);
		void init(SPR& spr);
		const String mName;
		Ogre::SceneNode* mSceneNode;
		Ogre::BillboardSet* mBillboardSet;
		Ogre::Billboard* mSprite;
		Ogre::Material* mMaterial;
		Ogre::Pass* mPass;
		Ogre::TextureUnitState* mTexUnit;
		Frames mFrames;
	};
}

#endif // __Sprite_H__
