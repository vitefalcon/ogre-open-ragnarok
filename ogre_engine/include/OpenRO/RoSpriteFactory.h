#pragma once
#ifndef __SpriteFactory_H__
#define __SpriteFactory_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoBaseObject.h>

namespace Gorilla
{
	class Screen;
	class ScreenRenderable;
}

namespace ro
{
	class SpriteFactory : public BaseObject
	{
	public:
		~SpriteFactory(void);

		Gorilla::Screen* getGuiScreen();

		Gorilla::ScreenRenderable* createSpriteScreen();
	private:
		friend class Application;
		SpriteFactory(const String& guiAtlasName);
		void init(Ogre::Viewport* viewport);

		//GorillaRootPtr mGorilla;
		const String mGuiAtlasName;
		Gorilla::Screen* mGuiScreen;
	};
}

#endif // __SpriteFactory_H__
