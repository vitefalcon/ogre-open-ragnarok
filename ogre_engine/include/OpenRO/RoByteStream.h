#pragma once
#include <OpenRO/RoPrerequisites.h>
#include <sstream>

namespace ro
{
	class ByteWriteStream
	{
	public:
		ByteWriteStream(void);
		~ByteWriteStream(void);

		void writeInt8(int8 val);
		void writeUInt8(uint8 val);
		void writeInt16(int16 val);
		void writeUInt16(uint16 val);
		void writeInt32(int32 val);
		void writeUInt32(uint32 val);
		void writeFloat(float val);
		void writeFixedString(const std::string& val, size_t size);
		void writeString(const std::string& val);

		void getBuffer(byte** buffer, size_t& length);
	private:
		std::stringstream mStream;
	};
}
