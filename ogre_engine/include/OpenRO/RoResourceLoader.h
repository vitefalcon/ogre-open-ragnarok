#pragma once
#ifndef __ResourceLoader_H__
#define __ResourceLoader_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoSingleton.h>
#include <ro/types/grf.h>
#include <Ogre/OgreSharedPtr.h>
#include <Ogre/OgreDataStream.h>
#include <string>
#include <list>

namespace ro
{
	class ResourceLoader : public Singleton<ResourceLoader>
	{
	public:
		ResourceLoader(void);
		~ResourceLoader(void);

		void addGrf(const String& grf_file_path);

		Ogre::DataStreamPtr loadDataStream(const String& filename);

		InputStreamPtr loadInputStream(const String& filename);

		StringArray queryFiles(const String& regex, bool case_sensitive = false);
	private:
		typedef Ogre::SharedPtr<ro::GRF> GrfPtr;
		typedef std::vector<GrfPtr> GrfList;
		enum DataAllocator
		{
			ALLOC_NORMAL,
			ALLOC_OGRE
		};
		std::istream* loadFile( const String& filename );

		GrfList mGrfs;
	};
}

#endif // __ResourceLoader_H__
