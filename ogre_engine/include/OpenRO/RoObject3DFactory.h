#pragma once
#ifndef __Object3DFactory_H__
#define __Object3DFactory_H__

#include <OpenRO/RoPrerequisites.h>
#include <OpenRO/RoSingleton.h>

namespace ro
{
	class Object3DFactory : public Singleton<Object3DFactory>
	{
	public:
		~Object3DFactory(void);

		Object3DPtr load(const std::string& object_file_name);
		void unload(const std::string& object_file_name);
	private:
		friend class Application;

		Object3DFactory(void);

		typedef std::map<String, Object3DPtr> Object3DMap;
		Object3DMap mObjects;
	};
}

#endif // __Object3DFactory_H__
