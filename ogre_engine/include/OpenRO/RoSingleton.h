#pragma once
#ifndef __Singleton_H__
#define __Singleton_H__

#include <OpenRO/RoBaseObject.h>
#include <cassert>

namespace ro
{
	template <typename T>
	class Singleton : public BaseObject
	{
	public:
		static T& GetInstance()
		{
			assert(s_instance != 0);
			return *s_instance;
		}

		static T* GetInstancePtr()
		{
			return s_instance;
		}

	protected:
		Singleton()
		{
			assert(s_instance == 0);
#if defined( _MSC_VER ) && _MSC_VER < 1200	 
			int offset = (int)(T*)1 - (int)(Singleton <T>*)(T*)1;
			s_instance = (T*)((int)this + offset);
#else
			s_instance = static_cast< T* >( this );
#endif
		}

	private:
		static T* s_instance;
		Singleton(const Singleton&);
		const Singleton& operator = (const Singleton&);
	};
}

#endif // __Singleton_H__
