#pragma once
#ifndef __Prerequisites_H__
#define __Prerequisites_H__

#include <string>
#include <sstream>
#include <vector>
#include <list>

#include <Ogre/OgrePrerequisites.h>
#include <Ogre/OgreSharedPtr.h>

#ifndef __OPENRO_TRACK_MEMORY
#	ifdef _DEBUG
#		define __OPENRO_TRACK_MEMORY 1
#	else
#		define __OPENRO_TRACK_MEMORY 0
#	endif // _DEBUG
#endif // __OPENRO_TRACK_MEMORY

#define ROCOMPILER_MSVC			0x0001
#	define ROCOMPILER_MSVC_2003	0x0101
#	define ROCOMPILER_MSVC_2005	0x0201
#	define ROCOMPILER_MSVC_2008	0x0301
#	define ROCOMPILER_MSVC_2010	0x0401

#define ROCOMPILER_GCC			0x0002

#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#	if OGRE_COMPILER == OGRE_COMPILER_MSVC
#		if _MSC_VER >= 1600
#			define ROCOMPILER ROCOMPILER_MSVC_2010
#		elif _MSC_VER >= 1500
#			define ROCOMPILER ROCOMPILER_MSVC_2008
#		elif _MSC_VER >= 1400
#			define ROCOMPILER ROCOMPILER_MSVC_2005
#		elif _MSC_VER >= 1400
#			define ROCOMPILER ROCOMPILER_MSVC_2003
#		else
#			error Unknown compiler version
#		endif // _MSC_VER
#	endif // OGRE_COMPILER
#endif // OGRE_PLATFORM

namespace ro
{
	typedef unsigned char uchar;
	typedef unsigned char byte;
	typedef unsigned short ushort;
	typedef unsigned int uint;
	typedef unsigned long ulong;

#if _WIN32
	typedef __int8				int8;
	typedef unsigned __int8		uint8;
	typedef __int16				int16;
	typedef unsigned __int16	uint16;
	typedef __int32				int32;
	typedef unsigned __int32	uint32;
	typedef __int64				int64;
	typedef unsigned __int64	uint64;
#else
	typedef char				int8;
	typedef unsigned char		uint8;
	typedef short				int16;
	typedef unsigned short		uint16;
	typedef int					int32;
	typedef unsigned int		uint32;
	typedef long long			int64;
	typedef unsigned long long	uint64;
#endif // _WIN32

	typedef std::string String;
	typedef std::stringstream StringStream;
	typedef std::istream InputStream;
	typedef std::list<String> StringList;
	typedef std::vector<String> StringArray;

	// RO Types
	class Image;
	class Object3D;
	class Background;
	class ResourceLoader;
	class Object3DFactory;
//	class InputStream;

	// RO SharedPtr Types
	typedef Ogre::SharedPtr<Image> ImagePtr;
	typedef Ogre::SharedPtr<Object3D> Object3DPtr;
	typedef Ogre::SharedPtr<InputStream> InputStreamPtr;
}

#define LOG_CRITICAL(msg)do{\
	std::stringstream ss;ss<<msg;\
	Ogre::LogManager::getSingleton().logMessage(Ogre::LML_CRITICAL, ss.str(), false);\
	}while(false)

#endif // __Prerequisites_H__