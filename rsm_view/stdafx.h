#ifndef __STDAFX_H
#define __STDAFX_H

#ifdef WIN32
	#define _CRT_SECURE_NO_WARNINGS
	#define _SDL_main_h
#endif

#include "ro/ro_static.h"
#include "rogl/rogl_static.h"

#endif /* __STDAFX_H */